﻿using System;
using System.Windows.Forms;
using Npgsql;

namespace DiplomskiRad
{
    public partial class Prijava : Form
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        Repository.AdminRepository adminRepository = new Repository.AdminRepository();

        public Prijava()
        {
            InitializeComponent();
            adminRepository.KreirajDefaultAdmina();

            this.Show();
        }

        private void btnLogIn_Click_1(object sender, EventArgs e)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String uloga = "";
            Guid nalogId = new Guid();

            using (var cmd = new NpgsqlCommand("select id, uloga from diplomski.nalog " +
                "where username = '" + txtUsername.Text + "' and password = '" + txtPassword.Text + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    uloga = (String)reader.GetValue(1);
                    nalogId = (Guid)reader.GetValue(0);

                    MessageBox.Show("Uloga: " + uloga);
                    txtUsername.Text = "";
                    txtPassword.Text = "";
                }
                else
                {
                    MessageBox.Show("Pristup odbijen. Ne postoji nalog sa unetim vrednostima za korisničko ime i lozinku.");
                    txtUsername.Text = "";
                    txtPassword.Text = "";
                    conn.Close();
                    return;
                }

            conn.Close();

            if (uloga == "student")
            {
                StudentForm studentForm = new StudentForm();
                studentForm.prijavaForm = this;
                studentForm.UcitajStudenta(nalogId);
                studentForm.Show();
            }
            else if(uloga == "admin")
            {
                AdminForm adminForm = new AdminForm();
                adminForm.prijavaForm = this;
                adminForm.Show();
            }
            else
            {
                Form1 form1 = new Form1();
                form1.prijavaForm = this;
                form1.Ucitaj(uloga, nalogId);
                form1.Show();
            }
            this.Hide();
        }
    }
}
