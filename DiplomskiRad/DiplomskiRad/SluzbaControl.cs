﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace DiplomskiRad
{
    public partial class SluzbaControl : UserControl
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        Repository.DiplomskiRadRepository diplomskiRadRepository = new Repository.DiplomskiRadRepository();
        String stanjeSelektovanogRada;
        Guid idSelektovanogRada = new Guid();
        List<View.DiplomskiView> sviDiplomskiRadovi = new List<View.DiplomskiView>();
        public Guid nalogId = new Guid();
        Repository.StudentskaSluzbaRepository studentskaSluzbaRepository = new Repository.StudentskaSluzbaRepository();

        public SluzbaControl()
        {
            InitializeComponent();

            dgvDiplomskiRadovi.AllowUserToAddRows = false;

            cmbStanja.Items.Add("prijavljen");
            cmbStanja.Items.Add("mentor potvrdio završetak rada");
            cmbStanja.Items.Add("odbranjen");
            cmbStanja.Items.Add("ostalo");

            dgvDiplomskiRadovi.Visible = false;
            dgvDiplomskiRadovi.AllowUserToAddRows = false;

            btnOpcije.Visible = false;
            btnPrikazKomisije.Visible = false;
            lblKomisija.Visible = false;
        }

        private void SluzbaControl_Load(object sender, EventArgs e)
        {

        }

        public void InicijalizujPrikaz()
        {
            lblUloga.Text += "\n\n" + studentskaSluzbaRepository.VratiImeRadnika(this.nalogId);

            this.sviDiplomskiRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();

            if (this.sviDiplomskiRadovi.Count == 0)
            {
                lblIspredTabele.Text = "Trenutno nema prijavljenih diplomskih radova.";
                dgvDiplomskiRadovi.Visible = false;
                btnOpcije.Visible = false;

                lblOdabirStanja.Visible = false;
                cmbStanja.Visible = false;
            }
            else
            {
                lblIspredTabele.Visible = false;
                lblOdabirStanja.Visible = true;
                cmbStanja.Visible = true;
                cmbStanja.SelectedIndex = -1;
            }
        }

        private void btnOpcije_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    this.idSelektovanogRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["Id"].Value;

                    this.stanjeSelektovanogRada = cmbStanja.SelectedItem.ToString();

                    switch (stanjeSelektovanogRada)
                    {
                        case "prijavljen":
                            {
                                if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "služba izvršila proveru"))
                                {
                                    MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                    this.stanjeSelektovanogRada = "služba izvršila proveru";
                                    this.sviDiplomskiRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                    this.PrikaziTrazeneRadove();
                                }
                                else
                                    MessageBox.Show("Greška. Pokušajte ponovo.");
                            }
                            break;
                        case "mentor potvrdio završetak rada":
                            {
                                if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "služba izvršila proveru nakon potvrde mentora"))
                                {
                                    MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                    this.stanjeSelektovanogRada = "služba izvršila proveru nakon potvrde mentora";
                                    this.sviDiplomskiRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                    this.PrikaziTrazeneRadove();
                                }
                                else
                                    MessageBox.Show("Greška. Pokušajte ponovo.");
                            }
                            break;
                        case "odbranjen":
                            {
                                if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "služba izdala potvrdu"))
                                {
                                    MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                    this.stanjeSelektovanogRada = "služba izdala potvrdu";
                                    this.sviDiplomskiRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                    this.PrikaziTrazeneRadove();
                                }
                                else
                                    MessageBox.Show("Greška. Pokušajte ponovo.");
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else
                MessageBox.Show("Morate odabrati jedan rad!");
        }

        private void PrikaziTrazeneRadove()
        {
            btnPrikazKomisije.Visible = false;

            List<View.DiplomskiView> zaPrikaz = new List<View.DiplomskiView>();

            bool stanje = false;

            if (cmbStanja.SelectedIndex != 3)
            {
                foreach (View.DiplomskiView rad in this.sviDiplomskiRadovi)
                    if (rad.Stanje.Equals(cmbStanja.SelectedItem.ToString()))
                        zaPrikaz.Add(rad);

                if (cmbStanja.SelectedItem.Equals("prijavljen"))
                    btnOpcije.Text = "Potvrdite proveru";
                else
                        if (cmbStanja.SelectedItem.Equals("mentor potvrdio završetak rada"))
                    btnOpcije.Text = "Potvrdite proveru";
                else
                        if (cmbStanja.SelectedItem.Equals("odbranjen"))
                            btnOpcije.Text = "Potvrdite izdavanje potvrde";

                stanje = false;
                btnOpcije.Visible = true;
            }
            else
            {
                foreach (View.DiplomskiView rad in this.sviDiplomskiRadovi)
                    if (!rad.Stanje.Equals("prijavljen") &&
                        !rad.Stanje.Equals("mentor potvrdio završetak rada") &&
                        !rad.Stanje.Equals("odbranjen"))
                        zaPrikaz.Add(rad);

                btnOpcije.Visible = false;
                stanje = true;
            }

            if (zaPrikaz.Count == 0)
            {
                lblIspredTabele.Text = "Trenutno nema diplomskih radova u traženom stanju";
                lblIspredTabele.Visible = true;
                dgvDiplomskiRadovi.Visible = false;
                btnOpcije.Visible = false;
                btnPrikazKomisije.Visible = false;
                lblKomisija.Visible = false;
                btnPrikazKomisije.Visible = false;
            }
            else
            {
                dgvDiplomskiRadovi.DataSource = new BindingList<View.DiplomskiView>(zaPrikaz);

                this.dgvDiplomskiRadovi.Columns[1].HeaderText = "Ime studenta";
                this.dgvDiplomskiRadovi.Columns[2].HeaderText = "Broj indeksa studenta";
                this.dgvDiplomskiRadovi.Columns[5].HeaderText = "Ime mentora";
                this.dgvDiplomskiRadovi.Columns[6].HeaderText = "Stanje diplomskog rada";
                this.dgvDiplomskiRadovi.Columns[7].HeaderText = "Katedra";
                this.dgvDiplomskiRadovi.Columns[8].HeaderText = "Naziv diplomskog rada";
                this.dgvDiplomskiRadovi.Columns[9].HeaderText = "Naziv zadatka";
                this.dgvDiplomskiRadovi.Columns[12].HeaderText = "Datum odbrane";

                this.dgvDiplomskiRadovi.Columns["Id"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["Stanje"].Visible = stanje;
                this.dgvDiplomskiRadovi.Columns["MentorId"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["StudentId"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["ClanKomisije1"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["ClanKomisije2"].Visible = false;

                lblIspredTabele.Visible = false;
                dgvDiplomskiRadovi.Visible = true;

                if (cmbStanja.SelectedItem.Equals("odbranjen"))
                {
                    btnPrikazKomisije.Visible = true;
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = true;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                    dgvDiplomskiRadovi.Columns[12].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                else if (cmbStanja.SelectedItem.Equals("ostalo"))
                {
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = true;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
                else
                {
                    btnPrikazKomisije.Visible = false;
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = false;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                    dgvDiplomskiRadovi.Columns[9].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void cmbStanja_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblKomisija.Visible = false;

            if (cmbStanja.SelectedIndex != -1)
            {
                PrikaziTrazeneRadove();
                lblKomisija.Visible = false;
            }
        }

        private void btnPrikazKomisije_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                String prviClan = (String)dgvDiplomskiRadovi.SelectedRows[0].Cells["ClanKomisije1"].Value;
                String drugiClan = (String)dgvDiplomskiRadovi.SelectedRows[0].Cells["ClanKomisije2"].Value;

                if (prviClan != null)
                    lblKomisija.Text = "1. član komisije: " + prviClan +
                        "\n\n2. član komisije: " + drugiClan;
                else
                    lblKomisija.Text = "Šef katedre još uvek nije dodao članove komisije.";
                lblKomisija.Visible = true;
            }
            else
                MessageBox.Show("Morate odabrati jedan rad!");
        }

        private void dgvDiplomskiRadovi_SelectionChanged(object sender, EventArgs e)
        {
            lblKomisija.Visible = false;
        }
    }
}
