﻿using System;

namespace DiplomskiRad.Entiteti
{
    class StudentskaSluzbaRadnik
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
        public Guid NalogId { get; set; }
    }
}
