﻿using System;

namespace DiplomskiRad.Entiteti
{
    class Student
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
        public String BrojIndeksa { get; set; }
        public Guid NalogId { get; set; }
    }
} 
