﻿using System;

namespace DiplomskiRad.Entiteti
{
    class Prodekan
    {
        public Guid Id { get; set; }
        public String PunoIme { get; set; }
        public String Aktivan { get; set; }
        public Guid NalogId { get; set; }
    }
}
