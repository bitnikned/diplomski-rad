﻿using System;

namespace DiplomskiRad.Entiteti
{
    class Admin
    {
        public Guid Id { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
