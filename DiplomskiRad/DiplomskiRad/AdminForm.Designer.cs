﻿namespace DiplomskiRad
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.lblUloga = new System.Windows.Forms.Label();
            this.btnNazad = new System.Windows.Forms.Button();
            this.panelKreiranjeNaloga = new System.Windows.Forms.Panel();
            this.btnKreirajManji = new System.Windows.Forms.Button();
            this.cmbKatedre = new System.Windows.Forms.ComboBox();
            this.lblMentori = new System.Windows.Forms.Label();
            this.dgvMentori = new System.Windows.Forms.DataGridView();
            this.btnKreiraj = new System.Windows.Forms.Button();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtBrojIndeksa = new System.Windows.Forms.TextBox();
            this.lblPunoIme = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPunoIme = new System.Windows.Forms.TextBox();
            this.lblBrojIndeksailiKatedra = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.panelBrisanje = new System.Windows.Forms.Panel();
            this.btnBrisanje2 = new System.Windows.Forms.Button();
            this.btnBrisanje = new System.Windows.Forms.Button();
            this.dgvBrisanje = new System.Windows.Forms.DataGridView();
            this.lblIspredTabele = new System.Windows.Forms.Label();
            this.cmbUloga = new System.Windows.Forms.ComboBox();
            this.cmbOpcije = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelKatedre = new System.Windows.Forms.Panel();
            this.btnKatedre = new System.Windows.Forms.Button();
            this.txtKatedra = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelKreiranjeNaloga.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMentori)).BeginInit();
            this.panelBrisanje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBrisanje)).BeginInit();
            this.panelKatedre.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUloga
            // 
            this.lblUloga.AutoSize = true;
            this.lblUloga.Location = new System.Drawing.Point(158, 59);
            this.lblUloga.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUloga.Name = "lblUloga";
            this.lblUloga.Size = new System.Drawing.Size(45, 19);
            this.lblUloga.TabIndex = 21;
            this.lblUloga.Text = "Uloga";
            // 
            // btnNazad
            // 
            this.btnNazad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNazad.Location = new System.Drawing.Point(497, 544);
            this.btnNazad.Margin = new System.Windows.Forms.Padding(4);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(112, 34);
            this.btnNazad.TabIndex = 20;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseVisualStyleBackColor = false;
            this.btnNazad.Click += new System.EventHandler(this.btnNazad_Click);
            // 
            // panelKreiranjeNaloga
            // 
            this.panelKreiranjeNaloga.Controls.Add(this.btnKreirajManji);
            this.panelKreiranjeNaloga.Controls.Add(this.cmbKatedre);
            this.panelKreiranjeNaloga.Controls.Add(this.lblMentori);
            this.panelKreiranjeNaloga.Controls.Add(this.dgvMentori);
            this.panelKreiranjeNaloga.Controls.Add(this.btnKreiraj);
            this.panelKreiranjeNaloga.Controls.Add(this.txtUsername);
            this.panelKreiranjeNaloga.Controls.Add(this.lblUsername);
            this.panelKreiranjeNaloga.Controls.Add(this.txtBrojIndeksa);
            this.panelKreiranjeNaloga.Controls.Add(this.lblPunoIme);
            this.panelKreiranjeNaloga.Controls.Add(this.lblPassword);
            this.panelKreiranjeNaloga.Controls.Add(this.txtPunoIme);
            this.panelKreiranjeNaloga.Controls.Add(this.lblBrojIndeksailiKatedra);
            this.panelKreiranjeNaloga.Controls.Add(this.txtPassword);
            this.panelKreiranjeNaloga.Location = new System.Drawing.Point(6, 99);
            this.panelKreiranjeNaloga.Margin = new System.Windows.Forms.Padding(4);
            this.panelKreiranjeNaloga.Name = "panelKreiranjeNaloga";
            this.panelKreiranjeNaloga.Size = new System.Drawing.Size(614, 435);
            this.panelKreiranjeNaloga.TabIndex = 18;
            // 
            // btnKreirajManji
            // 
            this.btnKreirajManji.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnKreirajManji.Location = new System.Drawing.Point(317, 342);
            this.btnKreirajManji.Margin = new System.Windows.Forms.Padding(4);
            this.btnKreirajManji.Name = "btnKreirajManji";
            this.btnKreirajManji.Size = new System.Drawing.Size(157, 44);
            this.btnKreirajManji.TabIndex = 12;
            this.btnKreirajManji.Text = "Kreirajte nalog";
            this.btnKreirajManji.UseVisualStyleBackColor = false;
            this.btnKreirajManji.Click += new System.EventHandler(this.btnKreirajManji_Click);
            // 
            // cmbKatedre
            // 
            this.cmbKatedre.FormattingEnabled = true;
            this.cmbKatedre.Location = new System.Drawing.Point(263, 247);
            this.cmbKatedre.Name = "cmbKatedre";
            this.cmbKatedre.Size = new System.Drawing.Size(211, 27);
            this.cmbKatedre.TabIndex = 11;
            // 
            // lblMentori
            // 
            this.lblMentori.AutoSize = true;
            this.lblMentori.Location = new System.Drawing.Point(87, 8);
            this.lblMentori.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMentori.Name = "lblMentori";
            this.lblMentori.Size = new System.Drawing.Size(123, 19);
            this.lblMentori.TabIndex = 10;
            this.lblMentori.Text = "Odaberite mentora";
            // 
            // dgvMentori
            // 
            this.dgvMentori.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMentori.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvMentori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMentori.Location = new System.Drawing.Point(5, 31);
            this.dgvMentori.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMentori.Name = "dgvMentori";
            this.dgvMentori.Size = new System.Drawing.Size(605, 291);
            this.dgvMentori.TabIndex = 9;
            // 
            // btnKreiraj
            // 
            this.btnKreiraj.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnKreiraj.Location = new System.Drawing.Point(261, 351);
            this.btnKreiraj.Margin = new System.Windows.Forms.Padding(4);
            this.btnKreiraj.Name = "btnKreiraj";
            this.btnKreiraj.Size = new System.Drawing.Size(213, 80);
            this.btnKreiraj.TabIndex = 6;
            this.btnKreiraj.Text = "Kreiraj nalog";
            this.btnKreiraj.UseVisualStyleBackColor = false;
            this.btnKreiraj.Click += new System.EventHandler(this.btnKreiraj_Click);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(261, 53);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(4);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(211, 26);
            this.txtUsername.TabIndex = 2;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(154, 57);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(101, 19);
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "Korisničko ime";
            // 
            // txtBrojIndeksa
            // 
            this.txtBrojIndeksa.Location = new System.Drawing.Point(263, 246);
            this.txtBrojIndeksa.Margin = new System.Windows.Forms.Padding(4);
            this.txtBrojIndeksa.Name = "txtBrojIndeksa";
            this.txtBrojIndeksa.Size = new System.Drawing.Size(211, 26);
            this.txtBrojIndeksa.TabIndex = 5;
            // 
            // lblPunoIme
            // 
            this.lblPunoIme.AutoSize = true;
            this.lblPunoIme.Location = new System.Drawing.Point(154, 183);
            this.lblPunoIme.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPunoIme.Name = "lblPunoIme";
            this.lblPunoIme.Size = new System.Drawing.Size(65, 19);
            this.lblPunoIme.TabIndex = 6;
            this.lblPunoIme.Text = "Puno ime";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(154, 119);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(57, 19);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Lozinka";
            // 
            // txtPunoIme
            // 
            this.txtPunoIme.Location = new System.Drawing.Point(261, 179);
            this.txtPunoIme.Margin = new System.Windows.Forms.Padding(4);
            this.txtPunoIme.Name = "txtPunoIme";
            this.txtPunoIme.Size = new System.Drawing.Size(211, 26);
            this.txtPunoIme.TabIndex = 4;
            // 
            // lblBrojIndeksailiKatedra
            // 
            this.lblBrojIndeksailiKatedra.AutoSize = true;
            this.lblBrojIndeksailiKatedra.Location = new System.Drawing.Point(156, 250);
            this.lblBrojIndeksailiKatedra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBrojIndeksailiKatedra.Name = "lblBrojIndeksailiKatedra";
            this.lblBrojIndeksailiKatedra.Size = new System.Drawing.Size(86, 19);
            this.lblBrojIndeksailiKatedra.TabIndex = 8;
            this.lblBrojIndeksailiKatedra.Text = "Broj indeksa";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(261, 114);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(211, 26);
            this.txtPassword.TabIndex = 3;
            // 
            // panelBrisanje
            // 
            this.panelBrisanje.Controls.Add(this.btnBrisanje2);
            this.panelBrisanje.Controls.Add(this.btnBrisanje);
            this.panelBrisanje.Controls.Add(this.dgvBrisanje);
            this.panelBrisanje.Controls.Add(this.lblIspredTabele);
            this.panelBrisanje.Location = new System.Drawing.Point(2, 103);
            this.panelBrisanje.Margin = new System.Windows.Forms.Padding(4);
            this.panelBrisanje.Name = "panelBrisanje";
            this.panelBrisanje.Size = new System.Drawing.Size(607, 342);
            this.panelBrisanje.TabIndex = 19;
            // 
            // btnBrisanje2
            // 
            this.btnBrisanje2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnBrisanje2.Location = new System.Drawing.Point(365, 304);
            this.btnBrisanje2.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrisanje2.Name = "btnBrisanje2";
            this.btnBrisanje2.Size = new System.Drawing.Size(112, 34);
            this.btnBrisanje2.TabIndex = 24;
            this.btnBrisanje2.Text = "Obrisati";
            this.btnBrisanje2.UseVisualStyleBackColor = false;
            this.btnBrisanje2.Click += new System.EventHandler(this.btnBrisanje2_Click_1);
            // 
            // btnBrisanje
            // 
            this.btnBrisanje.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnBrisanje.Location = new System.Drawing.Point(620, 351);
            this.btnBrisanje.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrisanje.Name = "btnBrisanje";
            this.btnBrisanje.Size = new System.Drawing.Size(112, 34);
            this.btnBrisanje.TabIndex = 2;
            this.btnBrisanje.Text = "Obriši";
            this.btnBrisanje.UseVisualStyleBackColor = false;
            // 
            // dgvBrisanje
            // 
            this.dgvBrisanje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBrisanje.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvBrisanje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBrisanje.Location = new System.Drawing.Point(4, 34);
            this.dgvBrisanje.Margin = new System.Windows.Forms.Padding(4);
            this.dgvBrisanje.Name = "dgvBrisanje";
            this.dgvBrisanje.Size = new System.Drawing.Size(599, 252);
            this.dgvBrisanje.TabIndex = 1;
            // 
            // lblIspredTabele
            // 
            this.lblIspredTabele.AutoSize = true;
            this.lblIspredTabele.Location = new System.Drawing.Point(86, 0);
            this.lblIspredTabele.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIspredTabele.Name = "lblIspredTabele";
            this.lblIspredTabele.Size = new System.Drawing.Size(238, 19);
            this.lblIspredTabele.TabIndex = 0;
            this.lblIspredTabele.Text = "Odaberite nalog koji želite da obrišete";
            // 
            // cmbUloga
            // 
            this.cmbUloga.FormattingEnabled = true;
            this.cmbUloga.Location = new System.Drawing.Point(225, 56);
            this.cmbUloga.Margin = new System.Windows.Forms.Padding(4);
            this.cmbUloga.Name = "cmbUloga";
            this.cmbUloga.Size = new System.Drawing.Size(254, 27);
            this.cmbUloga.TabIndex = 15;
            this.cmbUloga.SelectedIndexChanged += new System.EventHandler(this.cmbUloga_SelectedIndexChanged);
            // 
            // cmbOpcije
            // 
            this.cmbOpcije.FormattingEnabled = true;
            this.cmbOpcije.Location = new System.Drawing.Point(225, 21);
            this.cmbOpcije.Margin = new System.Windows.Forms.Padding(4);
            this.cmbOpcije.Name = "cmbOpcije";
            this.cmbOpcije.Size = new System.Drawing.Size(254, 27);
            this.cmbOpcije.TabIndex = 17;
            this.cmbOpcije.SelectedIndexChanged += new System.EventHandler(this.cmbOpcije_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 19);
            this.label1.TabIndex = 16;
            this.label1.Text = "Odaberite akciju";
            // 
            // panelKatedre
            // 
            this.panelKatedre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelKatedre.Controls.Add(this.btnKatedre);
            this.panelKatedre.Controls.Add(this.txtKatedra);
            this.panelKatedre.Controls.Add(this.label3);
            this.panelKatedre.Location = new System.Drawing.Point(40, 95);
            this.panelKatedre.Name = "panelKatedre";
            this.panelKatedre.Size = new System.Drawing.Size(465, 136);
            this.panelKatedre.TabIndex = 23;
            // 
            // btnKatedre
            // 
            this.btnKatedre.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnKatedre.Location = new System.Drawing.Point(334, 77);
            this.btnKatedre.Name = "btnKatedre";
            this.btnKatedre.Size = new System.Drawing.Size(104, 45);
            this.btnKatedre.TabIndex = 2;
            this.btnKatedre.Text = "Kreiraj";
            this.btnKatedre.UseVisualStyleBackColor = false;
            this.btnKatedre.Click += new System.EventHandler(this.btnKatedre_Click);
            // 
            // txtKatedra
            // 
            this.txtKatedra.Location = new System.Drawing.Point(188, 19);
            this.txtKatedra.Name = "txtKatedra";
            this.txtKatedra.Size = new System.Drawing.Size(252, 26);
            this.txtKatedra.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Unesite naziv katedre";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(623, 586);
            this.Controls.Add(this.panelKatedre);
            this.Controls.Add(this.lblUloga);
            this.Controls.Add(this.btnNazad);
            this.Controls.Add(this.panelBrisanje);
            this.Controls.Add(this.panelKreiranjeNaloga);
            this.Controls.Add(this.cmbUloga);
            this.Controls.Add(this.cmbOpcije);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Admin_FormClosed);
            this.panelKreiranjeNaloga.ResumeLayout(false);
            this.panelKreiranjeNaloga.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMentori)).EndInit();
            this.panelBrisanje.ResumeLayout(false);
            this.panelBrisanje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBrisanje)).EndInit();
            this.panelKatedre.ResumeLayout(false);
            this.panelKatedre.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblUloga;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Panel panelKreiranjeNaloga;
        private System.Windows.Forms.ComboBox cmbKatedre;
        private System.Windows.Forms.Label lblMentori;
        private System.Windows.Forms.DataGridView dgvMentori;
        private System.Windows.Forms.Button btnKreiraj;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtBrojIndeksa;
        private System.Windows.Forms.Label lblPunoIme;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPunoIme;
        private System.Windows.Forms.Label lblBrojIndeksailiKatedra;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Panel panelBrisanje;
        private System.Windows.Forms.Button btnBrisanje;
        private System.Windows.Forms.DataGridView dgvBrisanje;
        private System.Windows.Forms.Label lblIspredTabele;
        private System.Windows.Forms.ComboBox cmbUloga;
        private System.Windows.Forms.ComboBox cmbOpcije;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelKatedre;
        private System.Windows.Forms.Button btnKatedre;
        private System.Windows.Forms.TextBox txtKatedra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnKreirajManji;
        private System.Windows.Forms.Button btnBrisanje2;
    }
}