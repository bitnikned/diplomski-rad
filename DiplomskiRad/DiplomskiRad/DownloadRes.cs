﻿using System;

namespace DiplomskiRad
{
    class DownloadRes
    {
        public Guid Id { get; set; }
        public byte[] Dokument { get; set; }
        public string Naziv { get; set; }
        public string Datum { get; set; }
    }
}
