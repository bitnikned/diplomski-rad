﻿namespace DiplomskiRad
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOpcije = new System.Windows.Forms.Button();
            this.panelOpcije = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelPreuzimanje = new System.Windows.Forms.Panel();
            this.dgvDokumenti = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.btnOpen2 = new System.Windows.Forms.Button();
            this.txtDownload = new System.Windows.Forms.TextBox();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnPotvrdi = new System.Windows.Forms.Button();
            this.tlpPodaci = new System.Windows.Forms.TableLayoutPanel();
            this.lblNazivZadatka = new System.Windows.Forms.Label();
            this.lblNazivRada = new System.Windows.Forms.Label();
            this.lblKatedra = new System.Windows.Forms.Label();
            this.lblImeMentora = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblStanjeDiplomskog = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDatumOdbrane = new System.Windows.Forms.Label();
            this.lblKomisija = new System.Windows.Forms.Label();
            this.lblOpis = new System.Windows.Forms.Label();
            this.panelPrijava = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNazivZadatka = new System.Windows.Forms.TextBox();
            this.txtNazivRada = new System.Windows.Forms.TextBox();
            this.cmbMentori = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbKatedre = new System.Windows.Forms.ComboBox();
            this.btnPrijavi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panelOpcije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelPreuzimanje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDokumenti)).BeginInit();
            this.tlpPodaci.SuspendLayout();
            this.panelPrijava.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.btnOpcije);
            this.panel1.Controls.Add(this.panelOpcije);
            this.panel1.Controls.Add(this.tlpPodaci);
            this.panel1.Controls.Add(this.lblOpis);
            this.panel1.Controls.Add(this.panelPrijava);
            this.panel1.Location = new System.Drawing.Point(29, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(490, 490);
            this.panel1.TabIndex = 11;
            // 
            // btnOpcije
            // 
            this.btnOpcije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpcije.Location = new System.Drawing.Point(387, 30);
            this.btnOpcije.Name = "btnOpcije";
            this.btnOpcije.Size = new System.Drawing.Size(84, 35);
            this.btnOpcije.TabIndex = 24;
            this.btnOpcije.Text = "Opcije";
            this.btnOpcije.UseVisualStyleBackColor = false;
            this.btnOpcije.Click += new System.EventHandler(this.btnOpcije_Click);
            // 
            // panelOpcije
            // 
            this.panelOpcije.Controls.Add(this.pictureBox1);
            this.panelOpcije.Controls.Add(this.panelPreuzimanje);
            this.panelOpcije.Controls.Add(this.btnNazad);
            this.panelOpcije.Controls.Add(this.txtFile);
            this.panelOpcije.Controls.Add(this.btnOpen);
            this.panelOpcije.Controls.Add(this.btnPotvrdi);
            this.panelOpcije.Location = new System.Drawing.Point(0, 71);
            this.panelOpcije.Name = "panelOpcije";
            this.panelOpcije.Size = new System.Drawing.Size(487, 402);
            this.panelOpcije.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DiplomskiRad.Properties.Resources.graduation;
            this.pictureBox1.Location = new System.Drawing.Point(327, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // panelPreuzimanje
            // 
            this.panelPreuzimanje.Controls.Add(this.dgvDokumenti);
            this.panelPreuzimanje.Controls.Add(this.label14);
            this.panelPreuzimanje.Controls.Add(this.btnOpen2);
            this.panelPreuzimanje.Controls.Add(this.txtDownload);
            this.panelPreuzimanje.Controls.Add(this.btnDownload);
            this.panelPreuzimanje.Location = new System.Drawing.Point(2, 6);
            this.panelPreuzimanje.Name = "panelPreuzimanje";
            this.panelPreuzimanje.Size = new System.Drawing.Size(481, 254);
            this.panelPreuzimanje.TabIndex = 23;
            // 
            // dgvDokumenti
            // 
            this.dgvDokumenti.AllowUserToAddRows = false;
            this.dgvDokumenti.AllowUserToDeleteRows = false;
            this.dgvDokumenti.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvDokumenti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDokumenti.Location = new System.Drawing.Point(202, 48);
            this.dgvDokumenti.Name = "dgvDokumenti";
            this.dgvDokumenti.ReadOnly = true;
            this.dgvDokumenti.Size = new System.Drawing.Size(266, 138);
            this.dgvDokumenti.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 227);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(471, 19);
            this.label14.TabIndex = 25;
            this.label14.Text = "-----------------------------------------------------------------------------";
            // 
            // btnOpen2
            // 
            this.btnOpen2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpen2.Location = new System.Drawing.Point(14, 7);
            this.btnOpen2.Name = "btnOpen2";
            this.btnOpen2.Size = new System.Drawing.Size(158, 35);
            this.btnOpen2.TabIndex = 22;
            this.btnOpen2.Text = "Odaberite lokaciju";
            this.btnOpen2.UseVisualStyleBackColor = false;
            this.btnOpen2.Click += new System.EventHandler(this.btnOpen2_Click);
            // 
            // txtDownload
            // 
            this.txtDownload.Location = new System.Drawing.Point(202, 9);
            this.txtDownload.Name = "txtDownload";
            this.txtDownload.Size = new System.Drawing.Size(266, 26);
            this.txtDownload.TabIndex = 20;
            this.txtDownload.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnDownload.Location = new System.Drawing.Point(310, 192);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(158, 35);
            this.btnDownload.TabIndex = 16;
            this.btnDownload.Text = "Preuzmite rad";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNazad.Location = new System.Drawing.Point(386, 362);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(84, 35);
            this.btnNazad.TabIndex = 23;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseVisualStyleBackColor = false;
            this.btnNazad.Click += new System.EventHandler(this.btnNazad_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(205, 272);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(266, 26);
            this.txtFile.TabIndex = 13;
            this.txtFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpen.Location = new System.Drawing.Point(15, 266);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(158, 36);
            this.btnOpen.TabIndex = 15;
            this.btnOpen.Text = "Odaberite dokument";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnPotvrdi
            // 
            this.btnPotvrdi.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPotvrdi.Location = new System.Drawing.Point(312, 305);
            this.btnPotvrdi.Margin = new System.Windows.Forms.Padding(4);
            this.btnPotvrdi.Name = "btnPotvrdi";
            this.btnPotvrdi.Size = new System.Drawing.Size(158, 34);
            this.btnPotvrdi.TabIndex = 4;
            this.btnPotvrdi.Text = "Predajte rad";
            this.btnPotvrdi.UseVisualStyleBackColor = false;
            this.btnPotvrdi.Click += new System.EventHandler(this.btnPotvrdi_Click_1);
            // 
            // tlpPodaci
            // 
            this.tlpPodaci.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tlpPodaci.ColumnCount = 2;
            this.tlpPodaci.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.6494F));
            this.tlpPodaci.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.3506F));
            this.tlpPodaci.Controls.Add(this.lblNazivZadatka, 1, 4);
            this.tlpPodaci.Controls.Add(this.lblNazivRada, 1, 3);
            this.tlpPodaci.Controls.Add(this.lblKatedra, 1, 2);
            this.tlpPodaci.Controls.Add(this.lblImeMentora, 1, 1);
            this.tlpPodaci.Controls.Add(this.label9, 0, 4);
            this.tlpPodaci.Controls.Add(this.label8, 0, 3);
            this.tlpPodaci.Controls.Add(this.label12, 0, 0);
            this.tlpPodaci.Controls.Add(this.label7, 0, 2);
            this.tlpPodaci.Controls.Add(this.label6, 0, 1);
            this.tlpPodaci.Controls.Add(this.lblStanjeDiplomskog, 1, 0);
            this.tlpPodaci.Controls.Add(this.label11, 0, 5);
            this.tlpPodaci.Controls.Add(this.label10, 0, 6);
            this.tlpPodaci.Controls.Add(this.lblDatumOdbrane, 1, 6);
            this.tlpPodaci.Controls.Add(this.lblKomisija, 1, 5);
            this.tlpPodaci.Location = new System.Drawing.Point(3, 98);
            this.tlpPodaci.Name = "tlpPodaci";
            this.tlpPodaci.RowCount = 7;
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tlpPodaci.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tlpPodaci.Size = new System.Drawing.Size(484, 369);
            this.tlpPodaci.TabIndex = 5;
            // 
            // lblNazivZadatka
            // 
            this.lblNazivZadatka.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNazivZadatka.AutoSize = true;
            this.lblNazivZadatka.Location = new System.Drawing.Point(187, 212);
            this.lblNazivZadatka.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNazivZadatka.Name = "lblNazivZadatka";
            this.lblNazivZadatka.Size = new System.Drawing.Size(44, 19);
            this.lblNazivZadatka.TabIndex = 10;
            this.lblNazivZadatka.Text = "stanje";
            // 
            // lblNazivRada
            // 
            this.lblNazivRada.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNazivRada.AutoSize = true;
            this.lblNazivRada.Location = new System.Drawing.Point(187, 163);
            this.lblNazivRada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNazivRada.Name = "lblNazivRada";
            this.lblNazivRada.Size = new System.Drawing.Size(44, 19);
            this.lblNazivRada.TabIndex = 9;
            this.lblNazivRada.Text = "stanje";
            // 
            // lblKatedra
            // 
            this.lblKatedra.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblKatedra.AutoSize = true;
            this.lblKatedra.Location = new System.Drawing.Point(187, 116);
            this.lblKatedra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKatedra.Name = "lblKatedra";
            this.lblKatedra.Size = new System.Drawing.Size(44, 19);
            this.lblKatedra.TabIndex = 8;
            this.lblKatedra.Text = "stanje";
            // 
            // lblImeMentora
            // 
            this.lblImeMentora.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblImeMentora.AutoSize = true;
            this.lblImeMentora.Location = new System.Drawing.Point(187, 66);
            this.lblImeMentora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImeMentora.Name = "lblImeMentora";
            this.lblImeMentora.Size = new System.Drawing.Size(44, 19);
            this.lblImeMentora.TabIndex = 7;
            this.lblImeMentora.Text = "stanje";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 212);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 19);
            this.label9.TabIndex = 5;
            this.label9.Text = "Naziv zadatka";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(149, 19);
            this.label8.TabIndex = 5;
            this.label8.Text = "Naziv diplomskog rada";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 19);
            this.label12.TabIndex = 6;
            this.label12.Text = "Trenutno stanje rada";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 19);
            this.label7.TabIndex = 5;
            this.label7.Text = "Katedra";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ime mentora";
            // 
            // lblStanjeDiplomskog
            // 
            this.lblStanjeDiplomskog.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblStanjeDiplomskog.AutoSize = true;
            this.lblStanjeDiplomskog.Location = new System.Drawing.Point(187, 16);
            this.lblStanjeDiplomskog.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStanjeDiplomskog.Name = "lblStanjeDiplomskog";
            this.lblStanjeDiplomskog.Size = new System.Drawing.Size(44, 19);
            this.lblStanjeDiplomskog.TabIndex = 2;
            this.lblStanjeDiplomskog.Text = "stanje";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 273);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 19);
            this.label11.TabIndex = 5;
            this.label11.Text = "Članovi komisije";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 333);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 19);
            this.label10.TabIndex = 5;
            this.label10.Text = "Datum odbrane";
            // 
            // lblDatumOdbrane
            // 
            this.lblDatumOdbrane.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDatumOdbrane.AutoSize = true;
            this.lblDatumOdbrane.Location = new System.Drawing.Point(187, 333);
            this.lblDatumOdbrane.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDatumOdbrane.Name = "lblDatumOdbrane";
            this.lblDatumOdbrane.Size = new System.Drawing.Size(44, 19);
            this.lblDatumOdbrane.TabIndex = 11;
            this.lblDatumOdbrane.Text = "stanje";
            // 
            // lblKomisija
            // 
            this.lblKomisija.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblKomisija.AutoSize = true;
            this.lblKomisija.Location = new System.Drawing.Point(187, 273);
            this.lblKomisija.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKomisija.Name = "lblKomisija";
            this.lblKomisija.Size = new System.Drawing.Size(44, 19);
            this.lblKomisija.TabIndex = 12;
            this.lblKomisija.Text = "stanje";
            // 
            // lblOpis
            // 
            this.lblOpis.AutoSize = true;
            this.lblOpis.Location = new System.Drawing.Point(8, 15);
            this.lblOpis.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOpis.Name = "lblOpis";
            this.lblOpis.Size = new System.Drawing.Size(201, 19);
            this.lblOpis.TabIndex = 0;
            this.lblOpis.Text = "Ovde ispisani podaci o studentu";
            // 
            // panelPrijava
            // 
            this.panelPrijava.Controls.Add(this.label13);
            this.panelPrijava.Controls.Add(this.label5);
            this.panelPrijava.Controls.Add(this.label4);
            this.panelPrijava.Controls.Add(this.txtNazivZadatka);
            this.panelPrijava.Controls.Add(this.txtNazivRada);
            this.panelPrijava.Controls.Add(this.cmbMentori);
            this.panelPrijava.Controls.Add(this.label3);
            this.panelPrijava.Controls.Add(this.cmbKatedre);
            this.panelPrijava.Controls.Add(this.btnPrijavi);
            this.panelPrijava.Controls.Add(this.label1);
            this.panelPrijava.Location = new System.Drawing.Point(0, 80);
            this.panelPrijava.Margin = new System.Windows.Forms.Padding(4);
            this.panelPrijava.Name = "panelPrijava";
            this.panelPrijava.Size = new System.Drawing.Size(458, 370);
            this.panelPrijava.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(343, 19);
            this.label13.TabIndex = 10;
            this.label13.Text = "Niste prijavili diplomski rad, unesite podatke za prijavu.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 267);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Unesite naziv zadatka";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 193);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Unesite naziv diplomskog rada";
            // 
            // txtNazivZadatka
            // 
            this.txtNazivZadatka.Location = new System.Drawing.Point(207, 237);
            this.txtNazivZadatka.Multiline = true;
            this.txtNazivZadatka.Name = "txtNazivZadatka";
            this.txtNazivZadatka.Size = new System.Drawing.Size(241, 49);
            this.txtNazivZadatka.TabIndex = 7;
            // 
            // txtNazivRada
            // 
            this.txtNazivRada.Location = new System.Drawing.Point(207, 165);
            this.txtNazivRada.Multiline = true;
            this.txtNazivRada.Name = "txtNazivRada";
            this.txtNazivRada.Size = new System.Drawing.Size(241, 47);
            this.txtNazivRada.TabIndex = 6;
            // 
            // cmbMentori
            // 
            this.cmbMentori.FormattingEnabled = true;
            this.cmbMentori.Location = new System.Drawing.Point(207, 113);
            this.cmbMentori.Name = "cmbMentori";
            this.cmbMentori.Size = new System.Drawing.Size(241, 27);
            this.cmbMentori.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Odaberite katedru";
            // 
            // cmbKatedre
            // 
            this.cmbKatedre.FormattingEnabled = true;
            this.cmbKatedre.Location = new System.Drawing.Point(207, 65);
            this.cmbKatedre.Name = "cmbKatedre";
            this.cmbKatedre.Size = new System.Drawing.Size(241, 27);
            this.cmbKatedre.TabIndex = 3;
            this.cmbKatedre.SelectedIndexChanged += new System.EventHandler(this.cmbKatedre_SelectedIndexChanged_1);
            // 
            // btnPrijavi
            // 
            this.btnPrijavi.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPrijavi.Location = new System.Drawing.Point(336, 332);
            this.btnPrijavi.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrijavi.Name = "btnPrijavi";
            this.btnPrijavi.Size = new System.Drawing.Size(112, 34);
            this.btnPrijavi.TabIndex = 2;
            this.btnPrijavi.Text = "Prijavite";
            this.btnPrijavi.UseVisualStyleBackColor = false;
            this.btnPrijavi.Click += new System.EventHandler(this.btnPrijavi_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 121);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Odaberite mentora";
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(546, 531);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "StudentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Student_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelOpcije.ResumeLayout(false);
            this.panelOpcije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelPreuzimanje.ResumeLayout(false);
            this.panelPreuzimanje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDokumenti)).EndInit();
            this.tlpPodaci.ResumeLayout(false);
            this.tlpPodaci.PerformLayout();
            this.panelPrijava.ResumeLayout(false);
            this.panelPrijava.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblOpis;
        private System.Windows.Forms.Button btnPotvrdi;
        private System.Windows.Forms.Label lblStanjeDiplomskog;
        private System.Windows.Forms.Panel panelPrijava;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNazivZadatka;
        private System.Windows.Forms.TextBox txtNazivRada;
        private System.Windows.Forms.ComboBox cmbMentori;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbKatedre;
        private System.Windows.Forms.Button btnPrijavi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tlpPodaci;
        private System.Windows.Forms.Label lblKomisija;
        private System.Windows.Forms.Label lblDatumOdbrane;
        private System.Windows.Forms.Label lblNazivZadatka;
        private System.Windows.Forms.Label lblNazivRada;
        private System.Windows.Forms.Label lblKatedra;
        private System.Windows.Forms.Label lblImeMentora;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.TextBox txtDownload;
        private System.Windows.Forms.Button btnOpen2;
        private System.Windows.Forms.Panel panelOpcije;
        private System.Windows.Forms.Panel panelPreuzimanje;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.Button btnOpcije;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvDokumenti;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}