﻿namespace DiplomskiRad
{
    partial class MentorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIspredTabele = new System.Windows.Forms.Label();
            this.dgvDiplomskiRadovi = new System.Windows.Forms.DataGridView();
            this.btnOpcije = new System.Windows.Forms.Button();
            this.dtpDatumOdbrane = new System.Windows.Forms.DateTimePicker();
            this.panelClanovi = new System.Windows.Forms.Panel();
            this.cmbClan2 = new System.Windows.Forms.ComboBox();
            this.cmbClan1 = new System.Windows.Forms.ComboBox();
            this.btnClanovi = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPrikazKomisije = new System.Windows.Forms.Button();
            this.lblUloga = new System.Windows.Forms.Label();
            this.lblStanja = new System.Windows.Forms.Label();
            this.grpbOpcija = new System.Windows.Forms.GroupBox();
            this.rdbSamoZaMentora = new System.Windows.Forms.RadioButton();
            this.rdbSve = new System.Windows.Forms.RadioButton();
            this.cmbStanja = new System.Windows.Forms.ComboBox();
            this.btnVrati = new System.Windows.Forms.Button();
            this.lblKomisija = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnPanel = new System.Windows.Forms.Button();
            this.panelPreuzimanje = new System.Windows.Forms.Panel();
            this.dgvDokumenti = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnNazad = new System.Windows.Forms.Button();
            this.btnOpen2 = new System.Windows.Forms.Button();
            this.txtDownload = new System.Windows.Forms.TextBox();
            this.btnDownload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiplomskiRadovi)).BeginInit();
            this.panelClanovi.SuspendLayout();
            this.grpbOpcija.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelPreuzimanje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDokumenti)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIspredTabele
            // 
            this.lblIspredTabele.AutoSize = true;
            this.lblIspredTabele.Location = new System.Drawing.Point(100, 176);
            this.lblIspredTabele.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIspredTabele.Name = "lblIspredTabele";
            this.lblIspredTabele.Size = new System.Drawing.Size(175, 19);
            this.lblIspredTabele.TabIndex = 0;
            this.lblIspredTabele.Text = "Prijavljeni diplomski radovi:";
            // 
            // dgvDiplomskiRadovi
            // 
            this.dgvDiplomskiRadovi.AllowUserToAddRows = false;
            this.dgvDiplomskiRadovi.AllowUserToDeleteRows = false;
            this.dgvDiplomskiRadovi.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvDiplomskiRadovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiplomskiRadovi.Location = new System.Drawing.Point(4, 155);
            this.dgvDiplomskiRadovi.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDiplomskiRadovi.Name = "dgvDiplomskiRadovi";
            this.dgvDiplomskiRadovi.ReadOnly = true;
            this.dgvDiplomskiRadovi.Size = new System.Drawing.Size(975, 239);
            this.dgvDiplomskiRadovi.TabIndex = 1;
            this.dgvDiplomskiRadovi.SelectionChanged += new System.EventHandler(this.dgvDiplomskiRadovi_SelectionChanged);
            // 
            // btnOpcije
            // 
            this.btnOpcije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpcije.Location = new System.Drawing.Point(86, 504);
            this.btnOpcije.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpcije.Name = "btnOpcije";
            this.btnOpcije.Size = new System.Drawing.Size(167, 54);
            this.btnOpcije.TabIndex = 3;
            this.btnOpcije.Text = "Potvrda";
            this.btnOpcije.UseVisualStyleBackColor = false;
            this.btnOpcije.Click += new System.EventHandler(this.btnOpcije_Click);
            // 
            // dtpDatumOdbrane
            // 
            this.dtpDatumOdbrane.Location = new System.Drawing.Point(12, 460);
            this.dtpDatumOdbrane.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDatumOdbrane.Name = "dtpDatumOdbrane";
            this.dtpDatumOdbrane.Size = new System.Drawing.Size(241, 26);
            this.dtpDatumOdbrane.TabIndex = 3;
            // 
            // panelClanovi
            // 
            this.panelClanovi.Controls.Add(this.cmbClan2);
            this.panelClanovi.Controls.Add(this.cmbClan1);
            this.panelClanovi.Controls.Add(this.btnClanovi);
            this.panelClanovi.Controls.Add(this.label2);
            this.panelClanovi.Controls.Add(this.label3);
            this.panelClanovi.Location = new System.Drawing.Point(335, 418);
            this.panelClanovi.Margin = new System.Windows.Forms.Padding(4);
            this.panelClanovi.Name = "panelClanovi";
            this.panelClanovi.Size = new System.Drawing.Size(362, 182);
            this.panelClanovi.TabIndex = 6;
            // 
            // cmbClan2
            // 
            this.cmbClan2.FormattingEnabled = true;
            this.cmbClan2.Location = new System.Drawing.Point(136, 61);
            this.cmbClan2.Name = "cmbClan2";
            this.cmbClan2.Size = new System.Drawing.Size(222, 27);
            this.cmbClan2.TabIndex = 10;
            // 
            // cmbClan1
            // 
            this.cmbClan1.FormattingEnabled = true;
            this.cmbClan1.Location = new System.Drawing.Point(136, 4);
            this.cmbClan1.Name = "cmbClan1";
            this.cmbClan1.Size = new System.Drawing.Size(222, 27);
            this.cmbClan1.TabIndex = 9;
            // 
            // btnClanovi
            // 
            this.btnClanovi.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnClanovi.Location = new System.Drawing.Point(258, 139);
            this.btnClanovi.Margin = new System.Windows.Forms.Padding(4);
            this.btnClanovi.Name = "btnClanovi";
            this.btnClanovi.Size = new System.Drawing.Size(100, 39);
            this.btnClanovi.TabIndex = 8;
            this.btnClanovi.Text = "Dodajte";
            this.btnClanovi.UseVisualStyleBackColor = false;
            this.btnClanovi.Click += new System.EventHandler(this.btnClanovi_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "1. član komisije";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "2. član komisije";
            // 
            // btnPrikazKomisije
            // 
            this.btnPrikazKomisije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPrikazKomisije.Location = new System.Drawing.Point(334, 504);
            this.btnPrikazKomisije.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrikazKomisije.Name = "btnPrikazKomisije";
            this.btnPrikazKomisije.Size = new System.Drawing.Size(167, 54);
            this.btnPrikazKomisije.TabIndex = 13;
            this.btnPrikazKomisije.Text = "Članovi komisije";
            this.btnPrikazKomisije.UseVisualStyleBackColor = false;
            this.btnPrikazKomisije.Click += new System.EventHandler(this.btnPrikazKomisije_Click);
            // 
            // lblUloga
            // 
            this.lblUloga.AutoSize = true;
            this.lblUloga.Location = new System.Drawing.Point(29, 11);
            this.lblUloga.Name = "lblUloga";
            this.lblUloga.Size = new System.Drawing.Size(45, 19);
            this.lblUloga.TabIndex = 7;
            this.lblUloga.Text = "label1";
            // 
            // lblStanja
            // 
            this.lblStanja.AutoSize = true;
            this.lblStanja.Location = new System.Drawing.Point(100, 100);
            this.lblStanja.Name = "lblStanja";
            this.lblStanja.Size = new System.Drawing.Size(282, 19);
            this.lblStanja.TabIndex = 9;
            this.lblStanja.Text = "Odaberite stanje diplomskih radova za prikaz";
            // 
            // grpbOpcija
            // 
            this.grpbOpcija.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grpbOpcija.Controls.Add(this.rdbSamoZaMentora);
            this.grpbOpcija.Controls.Add(this.rdbSve);
            this.grpbOpcija.Location = new System.Drawing.Point(104, 23);
            this.grpbOpcija.Name = "grpbOpcija";
            this.grpbOpcija.Size = new System.Drawing.Size(532, 51);
            this.grpbOpcija.TabIndex = 10;
            this.grpbOpcija.TabStop = false;
            // 
            // rdbSamoZaMentora
            // 
            this.rdbSamoZaMentora.AutoSize = true;
            this.rdbSamoZaMentora.Location = new System.Drawing.Point(6, 22);
            this.rdbSamoZaMentora.Name = "rdbSamoZaMentora";
            this.rdbSamoZaMentora.Size = new System.Drawing.Size(230, 23);
            this.rdbSamoZaMentora.TabIndex = 1;
            this.rdbSamoZaMentora.TabStop = true;
            this.rdbSamoZaMentora.Text = "Prikaz radova kojima sam mentor";
            this.rdbSamoZaMentora.UseVisualStyleBackColor = true;
            this.rdbSamoZaMentora.CheckedChanged += new System.EventHandler(this.rdbSamoZaMentora_CheckedChanged);
            // 
            // rdbSve
            // 
            this.rdbSve.AutoSize = true;
            this.rdbSve.Location = new System.Drawing.Point(284, 22);
            this.rdbSve.Name = "rdbSve";
            this.rdbSve.Size = new System.Drawing.Size(242, 23);
            this.rdbSve.TabIndex = 0;
            this.rdbSve.TabStop = true;
            this.rdbSve.Text = "Prikaz svih ostalih dostupnih radova";
            this.rdbSve.UseVisualStyleBackColor = true;
            this.rdbSve.CheckedChanged += new System.EventHandler(this.rdbSve_CheckedChanged);
            // 
            // cmbStanja
            // 
            this.cmbStanja.FormattingEnabled = true;
            this.cmbStanja.Location = new System.Drawing.Point(388, 97);
            this.cmbStanja.Name = "cmbStanja";
            this.cmbStanja.Size = new System.Drawing.Size(248, 27);
            this.cmbStanja.TabIndex = 11;
            this.cmbStanja.SelectedIndexChanged += new System.EventHandler(this.cmbStanja_SelectedIndexChanged);
            // 
            // btnVrati
            // 
            this.btnVrati.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnVrati.Location = new System.Drawing.Point(802, 96);
            this.btnVrati.Margin = new System.Windows.Forms.Padding(4);
            this.btnVrati.Name = "btnVrati";
            this.btnVrati.Size = new System.Drawing.Size(158, 38);
            this.btnVrati.TabIndex = 12;
            this.btnVrati.Text = "Vratite rad";
            this.btnVrati.UseVisualStyleBackColor = false;
            this.btnVrati.Click += new System.EventHandler(this.btnVrati_Click);
            // 
            // lblKomisija
            // 
            this.lblKomisija.AutoSize = true;
            this.lblKomisija.Location = new System.Drawing.Point(522, 504);
            this.lblKomisija.Name = "lblKomisija";
            this.lblKomisija.Size = new System.Drawing.Size(45, 19);
            this.lblKomisija.TabIndex = 14;
            this.lblKomisija.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.lblUloga);
            this.panel1.Location = new System.Drawing.Point(699, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(281, 71);
            this.panel1.TabIndex = 15;
            // 
            // btnPanel
            // 
            this.btnPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPanel.Location = new System.Drawing.Point(86, 430);
            this.btnPanel.Margin = new System.Windows.Forms.Padding(4);
            this.btnPanel.Name = "btnPanel";
            this.btnPanel.Size = new System.Drawing.Size(167, 56);
            this.btnPanel.TabIndex = 16;
            this.btnPanel.Text = "Preuzimanje/predaja dokumenta";
            this.btnPanel.UseVisualStyleBackColor = false;
            this.btnPanel.Click += new System.EventHandler(this.btnPanel_Click);
            // 
            // panelPreuzimanje
            // 
            this.panelPreuzimanje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPreuzimanje.Controls.Add(this.dgvDokumenti);
            this.panelPreuzimanje.Controls.Add(this.label1);
            this.panelPreuzimanje.Controls.Add(this.txtFile);
            this.panelPreuzimanje.Controls.Add(this.btnOpen);
            this.panelPreuzimanje.Controls.Add(this.btnNazad);
            this.panelPreuzimanje.Controls.Add(this.btnOpen2);
            this.panelPreuzimanje.Controls.Add(this.txtDownload);
            this.panelPreuzimanje.Controls.Add(this.btnVrati);
            this.panelPreuzimanje.Controls.Add(this.btnDownload);
            this.panelPreuzimanje.Location = new System.Drawing.Point(5, 409);
            this.panelPreuzimanje.Name = "panelPreuzimanje";
            this.panelPreuzimanje.Size = new System.Drawing.Size(975, 202);
            this.panelPreuzimanje.TabIndex = 24;
            // 
            // dgvDokumenti
            // 
            this.dgvDokumenti.AllowUserToAddRows = false;
            this.dgvDokumenti.AllowUserToDeleteRows = false;
            this.dgvDokumenti.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvDokumenti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDokumenti.Location = new System.Drawing.Point(201, 47);
            this.dgvDokumenti.Name = "dgvDokumenti";
            this.dgvDokumenti.ReadOnly = true;
            this.dgvDokumenti.Size = new System.Drawing.Size(266, 86);
            this.dgvDokumenti.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 171);
            this.label1.TabIndex = 28;
            this.label1.Text = "|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n|\r\n";
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(694, 11);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(266, 26);
            this.txtFile.TabIndex = 26;
            this.txtFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpen.Location = new System.Drawing.Point(506, 6);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(158, 36);
            this.btnOpen.TabIndex = 27;
            this.btnOpen.Text = "Odaberite dokument";
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnNazad
            // 
            this.btnNazad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnNazad.Location = new System.Drawing.Point(876, 162);
            this.btnNazad.Name = "btnNazad";
            this.btnNazad.Size = new System.Drawing.Size(84, 35);
            this.btnNazad.TabIndex = 24;
            this.btnNazad.Text = "Nazad";
            this.btnNazad.UseVisualStyleBackColor = false;
            this.btnNazad.Click += new System.EventHandler(this.btnNazad_Click);
            // 
            // btnOpen2
            // 
            this.btnOpen2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpen2.Location = new System.Drawing.Point(13, 6);
            this.btnOpen2.Name = "btnOpen2";
            this.btnOpen2.Size = new System.Drawing.Size(158, 35);
            this.btnOpen2.TabIndex = 22;
            this.btnOpen2.Text = "Odaberite lokaciju";
            this.btnOpen2.UseVisualStyleBackColor = false;
            this.btnOpen2.Click += new System.EventHandler(this.btnOpen2_Click);
            // 
            // txtDownload
            // 
            this.txtDownload.Location = new System.Drawing.Point(201, 11);
            this.txtDownload.Name = "txtDownload";
            this.txtDownload.Size = new System.Drawing.Size(266, 26);
            this.txtDownload.TabIndex = 20;
            this.txtDownload.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnDownload.Location = new System.Drawing.Point(309, 142);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(158, 38);
            this.btnDownload.TabIndex = 16;
            this.btnDownload.Text = "Preuzmite rad";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // MentorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panelPreuzimanje);
            this.Controls.Add(this.btnPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblKomisija);
            this.Controls.Add(this.btnPrikazKomisije);
            this.Controls.Add(this.cmbStanja);
            this.Controls.Add(this.grpbOpcija);
            this.Controls.Add(this.lblStanja);
            this.Controls.Add(this.dtpDatumOdbrane);
            this.Controls.Add(this.panelClanovi);
            this.Controls.Add(this.btnOpcije);
            this.Controls.Add(this.dgvDiplomskiRadovi);
            this.Controls.Add(this.lblIspredTabele);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MentorControl";
            this.Size = new System.Drawing.Size(987, 625);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiplomskiRadovi)).EndInit();
            this.panelClanovi.ResumeLayout(false);
            this.panelClanovi.PerformLayout();
            this.grpbOpcija.ResumeLayout(false);
            this.grpbOpcija.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelPreuzimanje.ResumeLayout(false);
            this.panelPreuzimanje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDokumenti)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIspredTabele;
        private System.Windows.Forms.DataGridView dgvDiplomskiRadovi;
        private System.Windows.Forms.Button btnOpcije;
        private System.Windows.Forms.DateTimePicker dtpDatumOdbrane;
        private System.Windows.Forms.Panel panelClanovi;
        private System.Windows.Forms.Button btnClanovi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUloga;
        private System.Windows.Forms.ComboBox cmbClan2;
        private System.Windows.Forms.ComboBox cmbClan1;
        private System.Windows.Forms.Label lblStanja;
        private System.Windows.Forms.GroupBox grpbOpcija;
        private System.Windows.Forms.RadioButton rdbSamoZaMentora;
        private System.Windows.Forms.RadioButton rdbSve;
        private System.Windows.Forms.ComboBox cmbStanja;
        private System.Windows.Forms.Button btnVrati;
        private System.Windows.Forms.Button btnPrikazKomisije;
        private System.Windows.Forms.Label lblKomisija;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPanel;
        private System.Windows.Forms.Panel panelPreuzimanje;
        private System.Windows.Forms.Button btnOpen2;
        private System.Windows.Forms.TextBox txtDownload;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnNazad;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDokumenti;
    }
}
