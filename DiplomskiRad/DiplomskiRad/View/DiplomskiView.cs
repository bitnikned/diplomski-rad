﻿using System;

namespace DiplomskiRad.View
{
    class DiplomskiView
    {
        public Guid Id { get; set; }
        public String ImeStudenta { get; set; }
        public String BrojIndeksaStudenta { get; set; }
        public Guid MentorId { get; set; }
        public Guid StudentId { get; set; }
        public String ImeMentora { get; set; }
        public String Stanje { get; set; }
        public String Katedra { get; set; }
        public String NazivDiplomskogRada { get; set; }
        public String NazivZadatka { get; set; }
        public String ClanKomisije1 { get; set; }
        public String ClanKomisije2 { get; set; }
        public String DatumOdbrane { get; set; }
    }
}
