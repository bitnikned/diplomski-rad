﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace DiplomskiRad
{
    public partial class StudentForm : Form
    {
        public Prijava prijavaForm;

        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        private Entiteti.Student student;

        Repository.StudentRepository studentRepository = new Repository.StudentRepository();
        Repository.DiplomskiRadRepository diplomskiRadRepository = new Repository.DiplomskiRadRepository();
        Repository.MentorRepository mentorRepository = new Repository.MentorRepository();
        Repository.AdminRepository adminRepository = new Repository.AdminRepository();

        List<View.MentorView> mentoriKatedreView = new List<View.MentorView>();

        String filePath = string.Empty;
        byte[] readText;
        string nazivDokumenta;
        String stanje;
        Guid idRada = new Guid();
        String nazivRada;

        public StudentForm()
        {
            InitializeComponent();
            
            panelPrijava.Visible = false;
            panelOpcije.Visible = false;
            btnOpcije.Visible = false;

            tlpPodaci.Visible = false;

            foreach (String katedra in adminRepository.VratiSveKatedre())
                cmbKatedre.Items.Add(katedra);

            panel1.BringToFront();
        }

        public void UcitajStudenta(Guid nalogId)
        {
            Entiteti.Student st = studentRepository.VratiStudenta(nalogId);

            if (st != null)
            {
                this.student = st;
                lblOpis.Text = "Student: " + student.PunoIme +
                        "\n\nBroj indeksa: " + student.BrojIndeksa;
            }
            else
                MessageBox.Show("Greška!");

            View.DiplomskiView diplomskiView = diplomskiRadRepository.VratiDiplomskiRadStudenta(student.Id);
            this.idRada = diplomskiView.Id;
            this.stanje = diplomskiView.Stanje;
            this.nazivRada = diplomskiView.NazivDiplomskogRada;

            if (stanje != null)
            {
                this.stanje = diplomskiView.Stanje;
                PopuniTabelu(diplomskiView);
                tlpPodaci.Visible = true;

                if (stanje == "mentor prihvatio" || stanje == "vraćen")
                    btnOpcije.Visible = true;
                else
                    btnOpcije.Visible = false;
            }
            else
                panelPrijava.Visible = true;
        }

        private void PopuniTabelu(View.DiplomskiView diplomskiView)
        {
            lblStanjeDiplomskog.Text = diplomskiView.Stanje;
            lblImeMentora.Text = diplomskiView.ImeMentora;
            lblKatedra.Text = diplomskiView.Katedra;
            lblNazivRada.Text = diplomskiView.NazivDiplomskogRada;
            lblNazivZadatka.Text = diplomskiView.NazivZadatka;
            lblDatumOdbrane.Text = (diplomskiView.DatumOdbrane != null ? diplomskiView.DatumOdbrane : "/");
            lblKomisija.Text = (diplomskiView.ClanKomisije1 != null ? "1. " + diplomskiView.ClanKomisije1 + "\n" 
                + "2. " + diplomskiView.ClanKomisije2 : "/");
        }

        private void Student_FormClosed(object sender, FormClosedEventArgs e)
        {
            prijavaForm.Show();
        }

        private void btnPrijavi_Click_1(object sender, EventArgs e)
        {
            if (cmbKatedre.SelectedIndex != -1 && cmbMentori.SelectedIndex != -1
                && txtNazivRada.Text != "" && txtNazivZadatka.Text != "")
            {
                Guid mentorId = this.mentoriKatedreView[cmbMentori.SelectedIndex].Id;
                String katedra = cmbKatedre.SelectedItem.ToString();
                String nazivRada = txtNazivRada.Text;
                String nazivZadatka = txtNazivZadatka.Text;

                if (diplomskiRadRepository.PrijaviDiplomskiRad(student.Id, mentorId, katedra,
                    nazivRada, nazivZadatka))
                {
                    MessageBox.Show("Uspešna prijava diplomskog rada.");
                    panelPrijava.Visible = false;

                    View.DiplomskiView diplomskiView = diplomskiRadRepository.VratiDiplomskiRadStudenta(student.Id);

                    PopuniTabelu(diplomskiView);
                    tlpPodaci.Visible = true;
                }
                else
                    MessageBox.Show("Greška pri prijavi rada. Pokušajte ponovo.");
            }
            else
                MessageBox.Show("Popunite sva polja");
        }

        private void btnPotvrdi_Click_1(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                if (txtFile.Text == "")
                    MessageBox.Show("Odaberite dokument.");
                else
                {
                    if (diplomskiRadRepository.PromeniStanjeDiplomskogStudenta(student.Id, "student predao rad")
                        && diplomskiRadRepository.Upload(this.idRada, this.student.Id, this.readText, this.nazivDokumenta))
                    {
                        View.DiplomskiView diplomskiView = diplomskiRadRepository.VratiDiplomskiRadStudenta(student.Id);

                        PopuniTabelu(diplomskiView);
                        tlpPodaci.Visible = true;

                        panelOpcije.Visible = false;
                        btnOpcije.Visible = false;

                        MessageBox.Show("Uspešno izvršena predaja rada.");
                    }
                    else
                    {
                        MessageBox.Show("Greška pri predaji rada.");
                        diplomskiRadRepository.PromeniStanjeDiplomskogStudenta(student.Id, "mentor prihvatio");
                    }
                }
            }
        }

        private void cmbKatedre_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cmbMentori.Items.Clear();

            if (cmbKatedre.SelectedIndex != -1)
            {
                this.mentoriKatedreView = mentorRepository.VratiSveMentoreKatedre(cmbKatedre.SelectedItem.ToString());
                foreach (View.MentorView view in mentoriKatedreView)
                    cmbMentori.Items.Add(view.PunoIme);
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.Filter = "txt files (*.txt)|*.txt";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.filePath = openFileDialog.FileName;
                    txtFile.Text = filePath;
                    this.nazivDokumenta = Path.GetFileName(filePath);

                    this.readText = File.ReadAllBytes(filePath);
                }
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (txtDownload.Text == "")
                MessageBox.Show("Odaberite lokaciju za preuzimanje.");
            else
            {
                if (dgvDokumenti.SelectedRows.Count == 1)
                {
                    Guid idDokumenta = (Guid)dgvDokumenti.SelectedRows[0].Cells["Id"].Value;
                    String nazivDokumenta = (String)dgvDokumenti.SelectedRows[0].Cells["Naziv"].Value;

                    DownloadRes downloadRes = diplomskiRadRepository.Download(idDokumenta);

                    if (downloadRes != null)
                    {
                        try
                        {
                            String path = txtDownload.Text + @"\" + nazivDokumenta;
                            File.WriteAllBytes(path, downloadRes.Dokument);

                            MessageBox.Show("Uspešno preuzimanje dokumenta.");
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Greška prilikom preuzimanja dokumenta.");
                        }
                    }
                }
                else
                    MessageBox.Show("Morate izabrati dokument!");
            }
        }

        private void btnOpen2_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    txtDownload.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }

        private void btnNazad_Click(object sender, EventArgs e)
        {
            panelOpcije.Visible = false;
        }

        private void btnOpcije_Click(object sender, EventArgs e)
        {
            if (this.stanje == "mentor prihvatio")
            {
                panelOpcije.Visible = true;
                panelPreuzimanje.Visible = false;
            }
            else if (this.stanje == "vraćen")
            {
                panelOpcije.Visible = true;

                panelPreuzimanje.Visible = true;
                pictureBox1.Visible = false;

                List<DownloadRes> dokumenti = diplomskiRadRepository.DownloadSve(this.idRada);
                dgvDokumenti.DataSource = new BindingList<DownloadRes>(dokumenti);
                dgvDokumenti.Columns["Id"].Visible = false;
                dgvDokumenti.Columns["Dokument"].Visible = false;
                dgvDokumenti.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                dgvDokumenti.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
    }
}
