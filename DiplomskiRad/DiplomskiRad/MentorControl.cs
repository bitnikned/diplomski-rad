﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace DiplomskiRad
{
    public partial class MentorControl : UserControl
    {
        Repository.DiplomskiRadRepository diplomskiRadRepository = new Repository.DiplomskiRadRepository();
        Repository.MentorRepository mentorRepository = new Repository.MentorRepository();

        String stanjeSelektovanogRada;
        Guid idSelektovanogRada = new Guid();
        Guid idMentoraSelektovanogRada = new Guid();
        Guid mentorId = new Guid();
        public String katedra;
        List<View.DiplomskiView> sviDostupniRadovi = new List<View.DiplomskiView>();

        byte[] readText;
        string nazivDokumenta;
        Guid idRada = new Guid();

        public enum Uloga
        {
            Mentor,
            MentorSefKatedre,
            MentorProdekan
        }
        public Uloga uloga;

        public MentorControl()
        {
            InitializeComponent();

            dtpDatumOdbrane.Visible = false;
            panelClanovi.Visible = false;
            dgvDiplomskiRadovi.AllowUserToAddRows = false;
            dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            dgvDiplomskiRadovi.Visible = false;

            btnOpcije.Visible = false;
            btnVrati.Visible = false;
            btnPanel.Visible = false;
            lblIspredTabele.Visible = false;

            btnPrikazKomisije.Visible = false;
            lblKomisija.Visible = false;
            panelPreuzimanje.Visible = false;
        }

        public void InicijalizujPrikaz(Guid mentorId)
        {
            this.mentorId = mentorId;

            if (uloga == Uloga.Mentor)
            {
                lblUloga.Text = "Prijavljeni ste kao: mentor \n\n" + mentorRepository.VratiImeMentora(this.mentorId);

                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(mentorId);

                lblStanja.Visible = true;
                cmbStanja.Visible = true;
                grpbOpcija.Visible = false;

                cmbStanja.Items.Add("služba izvršila proveru");
                cmbStanja.Items.Add("student predao rad");
                cmbStanja.Items.Add("prodekan potvrdio");
                cmbStanja.Items.Add("mentor zakazao odbranu rada");
                cmbStanja.Items.Add("ostalo");
            }
            else
            {
                if (uloga == Uloga.MentorSefKatedre)
                {
                    this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                    lblUloga.Text = "Prijavljeni ste kao: mentor/šef katedre \n\n" + mentorRepository.VratiImeMentora(this.mentorId);
                }
                else if (uloga == Uloga.MentorProdekan)
                {
                    this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                    lblUloga.Text = "Prijavljeni ste kao: mentor/prodekan \n\n" + mentorRepository.VratiImeMentora(this.mentorId);
                }

                lblStanja.Visible = false;
                cmbStanja.Visible = false;
                grpbOpcija.Visible = true;
            }
        }

        private void PrikaziTrazeneRadove()
        {
            dtpDatumOdbrane.Visible = false;

            List<View.DiplomskiView> zaPrikaz = new List<View.DiplomskiView>();

            if (rdbSamoZaMentora.Checked || uloga == Uloga.Mentor)
            {
                if (cmbStanja.SelectedIndex.Equals(0))
                    btnOpcije.Text = "Potvrdite";
                else
                if (cmbStanja.SelectedIndex.Equals(1))
                    btnOpcije.Text = "Potvrdite";
                else
                if (cmbStanja.SelectedIndex.Equals(2))
                {
                    btnOpcije.Text = "Zakažite odbranu";
                    dtpDatumOdbrane.Visible = true;
                }
                else
                if (cmbStanja.SelectedIndex.Equals(3))
                    btnOpcije.Text = "Potvrdite";
            }
            else if(rdbSve.Checked)
            {
                if(uloga==Uloga.MentorSefKatedre)
                {
                    btnPrikazKomisije.Text = "Dodavanje članova komisije";
                    btnPrikazKomisije.Visible = true;
                }
                else if(uloga==Uloga.MentorProdekan)
                {
                    if (cmbStanja.SelectedIndex.Equals(0))
                        btnOpcije.Text = "Potvrdite";
                }
            }

            bool stanje = false;

            if (!cmbStanja.SelectedItem.Equals("ostalo"))
            {
                foreach (View.DiplomskiView rad in this.sviDostupniRadovi)
                {
                    if (uloga == Uloga.Mentor)
                    {
                        if (rad.Stanje.Equals(cmbStanja.SelectedItem.ToString())
                            && rad.MentorId.Equals(this.mentorId))
                            zaPrikaz.Add(rad);
                    }
                    else if (uloga == Uloga.MentorSefKatedre || uloga == Uloga.MentorProdekan)
                    {
                        if (rdbSamoZaMentora.Checked)
                        {
                            if (rad.Stanje.Equals(cmbStanja.SelectedItem.ToString())
                                 && rad.MentorId.Equals(this.mentorId))
                                zaPrikaz.Add(rad);
                        }
                        else if (rdbSve.Checked)
                        {
                            if (rad.Stanje.Equals(cmbStanja.SelectedItem.ToString()))
                                //&& !rad.MentorId.Equals(this.mentorId))
                                zaPrikaz.Add(rad);
                        }
                    }
                }

                stanje = false;
                btnOpcije.Visible = true;
            }
            else
            {
                foreach (View.DiplomskiView rad in this.sviDostupniRadovi)
                {
                    if (uloga == Uloga.Mentor || rdbSamoZaMentora.Checked)
                    {
                        if (!rad.Stanje.Equals("služba izvršila proveru") &&
                        !rad.Stanje.Equals("student predao rad") &&
                        !rad.Stanje.Equals("prodekan potvrdio") &&
                        !rad.Stanje.Equals("mentor zakazao odbranu rada") &&
                        rad.MentorId.Equals(this.mentorId))
                            zaPrikaz.Add(rad);
                    }
                    else if (uloga == Uloga.MentorSefKatedre)
                    {
                        if (!rad.Stanje.Equals("služba izvršila proveru nakon potvrde mentora"))
                                //&& !rad.MentorId.Equals(this.mentorId))
                                zaPrikaz.Add(rad);
                    }
                    else if (uloga == Uloga.MentorProdekan)
                    {
                       if (!rad.Stanje.Equals("šef katedre dodao članove komisije"))
                                //&& !rad.MentorId.Equals(this.mentorId))
                                zaPrikaz.Add(rad);
                    }
                }

                btnOpcije.Visible = false;
                btnVrati.Visible = false;
                btnPanel.Visible = false;
                stanje = true;
            }

            if (zaPrikaz.Count == 0)
            {
                lblIspredTabele.Text = "Trenutno nema diplomskih radova u traženom stanju";
                lblIspredTabele.Visible = true;
                dgvDiplomskiRadovi.Visible = false;
                btnOpcije.Visible = false;
                btnVrati.Visible = false;
                btnPanel.Visible = false;
                panelClanovi.Visible = false;
                dtpDatumOdbrane.Visible = false;
                btnPrikazKomisije.Visible = false;
                lblKomisija.Visible = false;
            }
            else
            {
                if (cmbStanja.SelectedItem.Equals("student predao rad"))
                {
                    btnVrati.Visible = true;
                    btnPanel.Visible = true;
                }

                dgvDiplomskiRadovi.DataSource = new BindingList<View.DiplomskiView>(zaPrikaz);

                this.dgvDiplomskiRadovi.Columns[1].HeaderText = "Ime studenta";
                this.dgvDiplomskiRadovi.Columns[2].HeaderText = "Broj indeksa studenta";
                this.dgvDiplomskiRadovi.Columns[5].HeaderText = "Ime mentora";
                this.dgvDiplomskiRadovi.Columns[6].HeaderText = "Stanje diplomskog rada";
                this.dgvDiplomskiRadovi.Columns[7].HeaderText = "Katedra";
                this.dgvDiplomskiRadovi.Columns[8].HeaderText = "Naziv diplomskog rada";
                this.dgvDiplomskiRadovi.Columns[9].HeaderText = "Naziv zadatka";
                this.dgvDiplomskiRadovi.Columns[12].HeaderText = "Datum odbrane";

                this.dgvDiplomskiRadovi.Columns["Id"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["Stanje"].Visible = stanje;
                this.dgvDiplomskiRadovi.Columns["MentorId"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["StudentId"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["ClanKomisije1"].Visible = false;
                this.dgvDiplomskiRadovi.Columns["ClanKomisije2"].Visible = false;
                if (rdbSamoZaMentora.Checked || uloga == Uloga.Mentor)
                    this.dgvDiplomskiRadovi.Columns["ImeMentora"].Visible = false;

                lblIspredTabele.Visible = false;
                dgvDiplomskiRadovi.Visible = true;

                if (cmbStanja.SelectedItem.Equals("mentor zakazao odbranu rada"))
                {
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = true;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    dgvDiplomskiRadovi.Columns[12].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                else if (cmbStanja.SelectedItem.Equals("ostalo"))
                {
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = true;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
                else
                {
                    this.dgvDiplomskiRadovi.Columns["DatumOdbrane"].Visible = false;
                    dgvDiplomskiRadovi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    dgvDiplomskiRadovi.Columns[9].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                if (rdbSve.Checked && uloga == Uloga.MentorSefKatedre)
                {
                    if (cmbStanja.SelectedIndex.Equals(0))
                    {
                        //panelClanovi.Visible = true;
                        btnPrikazKomisije.Visible = true;
                        btnOpcije.Visible = false;
                    }
                    else if (cmbStanja.SelectedIndex.Equals(1))
                    {
                        btnPrikazKomisije.Visible = true;
                        btnPrikazKomisije.Text = "Članovi komisije";
                    }
                }
                else if(uloga == Uloga.Mentor || rdbSamoZaMentora.Checked)
                {
                    if (cmbStanja.SelectedItem.Equals("prodekan potvrdio")
                        || cmbStanja.SelectedItem.Equals("mentor zakazao odbranu rada")
                        || cmbStanja.SelectedItem.Equals("ostalo"))
                        btnPrikazKomisije.Visible = true;
                }
                else if(uloga==Uloga.MentorProdekan)
                {
                    if (cmbStanja.SelectedItem.Equals("šef katedre dodao članove komisije")
                        || cmbStanja.SelectedItem.Equals("ostalo"))
                        btnPrikazKomisije.Visible = true;
                }
                else btnPrikazKomisije.Visible = false;
            }
        }

        private void btnOpcije_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                this.idSelektovanogRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["Id"].Value;
                this.idMentoraSelektovanogRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["MentorId"].Value;

                this.stanjeSelektovanogRada = cmbStanja.SelectedItem.ToString();

                if (stanjeSelektovanogRada != null)
                {
                    if (uloga == Uloga.Mentor ||
                        uloga == Uloga.MentorSefKatedre && rdbSamoZaMentora.Checked ||
                        uloga == Uloga.MentorProdekan && rdbSamoZaMentora.Checked)
                    {
                        switch (stanjeSelektovanogRada)
                        {
                            case "služba izvršila proveru":
                                {
                                    DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.OK)
                                    {
                                        if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "mentor prihvatio"))
                                        {
                                            MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                            this.stanjeSelektovanogRada = "mentor prihvatio";
                                            if (uloga == Uloga.Mentor)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(mentorId);
                                            else if (uloga == Uloga.MentorSefKatedre)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                                            else
                                                if (uloga == Uloga.MentorProdekan)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                            PrikaziTrazeneRadove();
                                        }
                                        else
                                            MessageBox.Show("Greška. Pokušajte ponovo.");
                                    }
                                }
                                return;
                            case "student predao rad":
                                {
                                    DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.OK)
                                    {
                                        if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "mentor potvrdio završetak rada"))
                                        {
                                            MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                            this.stanjeSelektovanogRada = "mentor potvrdio završetak rada";
                                            if (uloga == Uloga.Mentor)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(mentorId);
                                            else if (uloga == Uloga.MentorSefKatedre)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                                            else
                                                if (uloga == Uloga.MentorProdekan)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                            PrikaziTrazeneRadove();
                                        }
                                        else
                                            MessageBox.Show("Greška. Pokušajte ponovo.");
                                    }
                                }
                                return;
                            case "prodekan potvrdio":
                                {
                                    if (dtpDatumOdbrane.Value < DateTime.Now)
                                        MessageBox.Show("Morate odabrati predstojeći datum.");
                                    else
                                    {
                                        DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                                        if (dialogResult == DialogResult.OK)
                                        {
                                            if (diplomskiRadRepository.DodavanjeDatumaOdbrane(this.idSelektovanogRada, dtpDatumOdbrane.Value.ToString("yyyy-MM-dd"))
                                                            && diplomskiRadRepository.PromeniStanjeDiplomskog(this.idSelektovanogRada, "mentor zakazao odbranu rada"))
                                            {
                                                MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                                this.stanjeSelektovanogRada = "mentor zakazao odbranu rada";
                                                if (uloga == Uloga.Mentor)
                                                    this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(mentorId);
                                                else if (uloga == Uloga.MentorSefKatedre)
                                                    this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                                                else
                                                    if (uloga == Uloga.MentorProdekan)
                                                    this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                                PrikaziTrazeneRadove();
                                                dtpDatumOdbrane.Visible = false;
                                            }
                                            else
                                                MessageBox.Show("Greška. Pokušajte ponovo.");
                                        }
                                    }
                                }
                                return;
                            case "mentor zakazao odbranu rada":
                                {
                                    DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.OK)
                                    {
                                        DateTime datumOdbrane = DateTime.Parse((String)dgvDiplomskiRadovi.SelectedRows[0].Cells["DatumOdbrane"].Value);

                                        if (datumOdbrane > DateTime.Now)
                                            MessageBox.Show("Još uvek nije završena odbrana.");
                                        else if (diplomskiRadRepository.PromeniStanjeDiplomskog(this.idSelektovanogRada, "odbranjen"))
                                        {
                                            MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                            dtpDatumOdbrane.Visible = false;

                                            this.stanjeSelektovanogRada = "odbranjen";
                                            if (uloga == Uloga.Mentor)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(mentorId);
                                            else if (uloga == Uloga.MentorSefKatedre)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                                            else
                                                if (uloga == Uloga.MentorProdekan)
                                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                            PrikaziTrazeneRadove();
                                        }
                                        else
                                            MessageBox.Show("Greška. Pokušajte ponovo.");
                                    }
                                }
                                return;
                            default:
                                break;
                        }
                    }
                    if (this.uloga == Uloga.MentorSefKatedre)
                    {
                        dtpDatumOdbrane.Visible = false;
                    }
                    else if (this.uloga == Uloga.MentorProdekan)
                    {
                        if (stanjeSelektovanogRada == "šef katedre dodao članove komisije")
                        {
                            DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                            if (dialogResult == DialogResult.OK)
                            {
                                if (stanjeSelektovanogRada == "šef katedre dodao članove komisije")
                                {
                                    if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "prodekan potvrdio"))
                                    {
                                        MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                                        this.stanjeSelektovanogRada = "prodekan potvrdio";
                                        this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();
                                        PrikaziTrazeneRadove();
                                    }
                                    else
                                        MessageBox.Show("Greška. Pokušajte ponovo.");
                                }
                            }
                        }
                        else
                            MessageBox.Show("Nemate mogućnost promene stanja diplomskog rada dok je u trenutno prikazanom stanju.");
                    }
                }
                else
                    MessageBox.Show("Greška. Pokušajte ponovo.");
            }
            else
                MessageBox.Show("Morate izabrati jedan rad!");
        }

        private void btnClanovi_Click_1(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    this.idSelektovanogRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["Id"].Value;

                    List<String> imenaClanovaKomisije = new List<String>();
                    imenaClanovaKomisije.Add(cmbClan1.SelectedItem.ToString());

                    if (cmbClan1.SelectedIndex != cmbClan2.SelectedIndex)
                    {
                        imenaClanovaKomisije.Add(cmbClan2.SelectedItem.ToString());

                        if (diplomskiRadRepository.DodavanjeClanovaKomisije(idSelektovanogRada, imenaClanovaKomisije))
                        {
                            panelClanovi.Visible = false;
                            if (diplomskiRadRepository.PromeniStanjeDiplomskog(idSelektovanogRada, "šef katedre dodao članove komisije"))
                            {
                                MessageBox.Show("Uspešno izvršeno dodavanje članova komisije.");

                                this.stanjeSelektovanogRada = "šef katedre dodao članove komisije";
                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                                this.PrikaziTrazeneRadove();
                            }
                            else
                                MessageBox.Show("Greška prilikom dodavanja članova. Pokušajte ponovo.");
                        }
                        else
                            MessageBox.Show("Greška. Pokušajte ponovo.");
                    }
                    else
                    {
                        MessageBox.Show("Ne možete odabrati dva ista člana komisije.");
                        cmbClan1.SelectedIndex = -1;
                        cmbClan2.SelectedIndex = -1;
                    }
                }
            }
            else
                MessageBox.Show("Morate izabrati jedan rad!");
        }

        private void rdbSve_CheckedChanged(object sender, EventArgs e)
        {
            dgvDiplomskiRadovi.Visible = false;
            cmbStanja.Items.Clear();
            cmbStanja.SelectedIndex = -1;
            cmbStanja.Text = "";
            btnOpcije.Visible = false;
            btnVrati.Visible = false;
            btnPanel.Visible = false;
            panelClanovi.Visible = false;
            lblIspredTabele.Visible = false;
            lblKomisija.Visible = false;
            btnPrikazKomisije.Visible = false;

            if (rdbSve.Checked)
            {
                if(this.uloga==Uloga.MentorSefKatedre)
                {
                    cmbStanja.Items.Add("služba izvršila proveru nakon potvrde mentora");
                    cmbStanja.Items.Add("ostalo");

                    lblStanja.Visible = true;
                    cmbStanja.Visible = true;
                }
                else if(this.uloga == Uloga.MentorProdekan)
                {
                    cmbStanja.Items.Add("šef katedre dodao članove komisije");
                    cmbStanja.Items.Add("ostalo");

                    lblStanja.Visible = true;
                    cmbStanja.Visible = true;
                }
            }
        }

        private void rdbSamoZaMentora_CheckedChanged(object sender, EventArgs e)
        {
            dgvDiplomskiRadovi.Visible = false;
            cmbStanja.Items.Clear();
            cmbStanja.SelectedIndex = -1;
            cmbStanja.Text = "";
            btnOpcije.Visible = false;
            btnVrati.Visible = false;
            btnPanel.Visible = false;
            panelClanovi.Visible = false;
            lblIspredTabele.Visible = false;
            lblKomisija.Visible = false;
            btnPrikazKomisije.Visible = false;

            if (rdbSamoZaMentora.Checked)
            {
                cmbStanja.Items.Add("služba izvršila proveru");
                cmbStanja.Items.Add("student predao rad");
                cmbStanja.Items.Add("prodekan potvrdio");
                cmbStanja.Items.Add("mentor zakazao odbranu rada");
                cmbStanja.Items.Add("ostalo");

                lblStanja.Visible = true;
                cmbStanja.Visible = true;
            }
        }

        private void cmbStanja_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnVrati.Visible = false;
            btnPanel.Visible = false;
            panelClanovi.Visible = false;
            lblKomisija.Visible = false;
            btnPrikazKomisije.Visible = false;

            if (cmbStanja.SelectedIndex != -1)
            {
                PrikaziTrazeneRadove();
            }
        }

        private void btnVrati_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    if (cmbStanja.SelectedItem.Equals("student predao rad"))
                    {
                        Guid idRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["Id"].Value;
                        Guid idStudenta = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["StudentId"].Value;

                        if (diplomskiRadRepository.PromeniStanjeDiplomskog(idRada, "vraćen")
                            && diplomskiRadRepository.Upload(idRada, idStudenta, this.readText, this.nazivDokumenta))
                        {
                            MessageBox.Show("Uspešno promenjeno stanje diplomskog rada.");
                            panelPreuzimanje.Visible = false;

                            this.stanjeSelektovanogRada = "vraćen";
                            if (uloga == Uloga.Mentor)
                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveMentora(this.mentorId);
                            else if (uloga == Uloga.MentorSefKatedre)
                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadoveSefaKatedre(this.katedra);
                            else
                                if (uloga == Uloga.MentorProdekan)
                                this.sviDostupniRadovi = diplomskiRadRepository.VratiSveDiplomskeRadove();

                            this.PrikaziTrazeneRadove();
                        }
                        else
                            MessageBox.Show("Greška. Pokušajte ponovo.");
                    }
                }
            }
            else
                MessageBox.Show("Morate odabrati jedan rad!");
        }

        private void btnPrikazKomisije_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                if (rdbSve.Checked && uloga == Uloga.MentorSefKatedre && cmbStanja.SelectedIndex.Equals(0))
                {
                    Guid mentorRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["MentorId"].Value;

                    List<View.MentorView> imenaMentoraKatedre = mentorRepository.VratiClanoveKomisijeKatedre(
                            this.katedra, mentorRada);

                    foreach (View.MentorView view in imenaMentoraKatedre)
                    {
                        cmbClan1.Items.Add(view.PunoIme);
                        cmbClan2.Items.Add(view.PunoIme);
                    }
                    panelClanovi.Visible = true;
                    btnPrikazKomisije.Visible = false;
                }
                else
                {
                    String prviClan = (String)dgvDiplomskiRadovi.SelectedRows[0].Cells["ClanKomisije1"].Value;
                    String drugiClan = (String)dgvDiplomskiRadovi.SelectedRows[0].Cells["ClanKomisije2"].Value;

                    if (prviClan != null)
                        lblKomisija.Text = "1. član komisije: " + prviClan +
                            "\n\n2. član komisije: " + drugiClan;
                    else
                        lblKomisija.Text = "Šef katedre još uvek nije dodao članove komisije.";
                    lblKomisija.Visible = true;
                }
            }
            else
                MessageBox.Show("Morate odabrati jedan rad!");
        }

        private void dgvDiplomskiRadovi_SelectionChanged(object sender, EventArgs e)
        {
            lblKomisija.Visible = false;
        }

        private void btnPanel_Click(object sender, EventArgs e)
        {
            if (dgvDiplomskiRadovi.SelectedRows.Count == 1)
            {
                panelPreuzimanje.Visible = true;

                this.idRada = (Guid)dgvDiplomskiRadovi.SelectedRows[0].Cells["Id"].Value;

                List<DownloadRes> dokumenti = diplomskiRadRepository.DownloadSve(this.idRada);
                dgvDokumenti.DataSource = new BindingList<DownloadRes>(dokumenti);
                dgvDokumenti.Columns["Id"].Visible = false;
                dgvDokumenti.Columns["Dokument"].Visible = false;
                dgvDokumenti.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                dgvDokumenti.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                btnPanel.Visible = false;
            }
            else
                MessageBox.Show("Morate izabrati jedan rad!");
        }

        private void btnNazad_Click(object sender, EventArgs e)
        {
            panelPreuzimanje.Visible = false;
            btnPanel.Visible = true;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            if (txtDownload.Text == "")
                MessageBox.Show("Odaberite lokaciju za preuzimanje.");
            else
            {
                if (dgvDokumenti.SelectedRows.Count == 1)
                {
                    Guid idDokumenta = (Guid)dgvDokumenti.SelectedRows[0].Cells["Id"].Value;
                    String nazivDokumenta = (String)dgvDokumenti.SelectedRows[0].Cells["Naziv"].Value;

                    DownloadRes downloadRes = diplomskiRadRepository.Download(idDokumenta);

                    if (downloadRes != null)
                    {
                        try
                        {
                            String path = txtDownload.Text + @"\" + nazivDokumenta;
                            File.WriteAllBytes(path, downloadRes.Dokument);

                            MessageBox.Show("Uspešno preuzimanje dokumenta.");
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show("Greška prilikom preuzimanja dokumenta.");
                        }
                    }
                }
                else
                    MessageBox.Show("Morate izabrati dokument!");
            }
        }

        private void btnOpen2_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    txtDownload.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.Filter = "txt files (*.txt)|*.txt";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String filePath = openFileDialog.FileName;
                    txtFile.Text = filePath;

                    this.nazivDokumenta = Path.GetFileName(filePath);

                    this.readText = File.ReadAllBytes(filePath);
                }
            }
        }
    }
}
