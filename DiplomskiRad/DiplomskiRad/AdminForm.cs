﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace DiplomskiRad
{
    public partial class AdminForm : Form
    {
        public Prijava prijavaForm;

        Repository.AdminRepository adminRepository = new Repository.AdminRepository();
        Repository.StudentRepository studentRepository = new Repository.StudentRepository();
        Repository.MentorRepository mentorRepository = new Repository.MentorRepository();
        Repository.StudentskaSluzbaRepository studentskaSluzbaRepository = new Repository.StudentskaSluzbaRepository();
        Repository.SefKatedreRepository sefKatedreRepository = new Repository.SefKatedreRepository();
        Repository.NalogRepository nalogRepository = new Repository.NalogRepository();
        Repository.ProdekanRepository prodekanRepository = new Repository.ProdekanRepository();
        Repository.DiplomskiRadRepository diplomskiRadRepository = new Repository.DiplomskiRadRepository();

        public AdminForm()
        {
            InitializeComponent();

            cmbOpcije.Items.Add("Kreiranje naloga");
            cmbOpcije.Items.Add("Brisanje naloga");
            cmbOpcije.Items.Add("Brisanje diplomskog rada");
            cmbOpcije.Items.Add("Kreiranje katedre");

            cmbUloga.Items.Add("Administrator");
            cmbUloga.Items.Add("Student");
            cmbUloga.Items.Add("Mentor");
            cmbUloga.Items.Add("Radnik u studentskoj službi");
            cmbUloga.Items.Add("Šef katedre");
            cmbUloga.Items.Add("Prodekan");

            panelKreiranjeNaloga.Visible = false;
            panelBrisanje.Visible = false;
            panelKatedre.Visible = false;

            btnNazad.Visible = false;

            lblUloga.Visible = false;
            cmbUloga.Visible = false;

            dgvBrisanje.AllowUserToAddRows = false;
            dgvMentori.AllowUserToAddRows = false;

            foreach(String katedra in adminRepository.VratiSveKatedre())
                cmbKatedre.Items.Add(katedra);

            cmbKatedre.Visible = false;
        }

        private void cmbOpcije_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbOpcije.SelectedIndex != -1)
            {
                btnNazad.Visible = true;
                panelBrisanje.Visible = false;
                panelKreiranjeNaloga.Visible = false;
                panelKatedre.Visible = false;
            }

            txtUsername.Text = "";
            txtPassword.Text = "";
            txtPunoIme.Text = "";
            txtBrojIndeksa.Text = "";
            cmbKatedre.SelectedIndex = -1;
            if (cmbUloga.SelectedIndex == 2)
                cmbKatedre.Visible = true;
            else
                cmbKatedre.Visible = false;

            btnNazad.Visible = true;

            if (cmbOpcije.SelectedIndex == -1)
            {
                lblUloga.Visible = false;
                cmbUloga.Visible = false;
            }
            else if (cmbOpcije.SelectedIndex == 2)
            {
                lblUloga.Visible = false;
                cmbUloga.Visible = false;

                panelKreiranjeNaloga.Visible = false;
                PrikaziTabelu("Diplomski rad");
                panelBrisanje.Visible = true;
                btnBrisanje2.Visible = true;
            }
            else if (cmbOpcije.SelectedIndex == 3)
            {
                lblUloga.Visible = false;
                cmbUloga.Visible = false;

                panelKatedre.Visible = true;
            }
            else
            {
                lblUloga.Visible = true;
                cmbUloga.Visible = true;
            }
        }

        private void PrikaziTabelu(String tabela)
        {
            lblIspredTabele.Text = "Odaberite nalog koji želite da obrišete.";
            dgvBrisanje.Visible = true;

            switch (tabela)
            {
                case "Administrator":
                    {
                        List<Entiteti.Admin> list = adminRepository.VratiSveAdmine();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje admini u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.Admin>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Student":
                    {
                        List<Entiteti.Student> list = studentRepository.VratiSveStudente();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje studenti u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.Student>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Radnik u studentskoj službi":
                    {
                        List<Entiteti.StudentskaSluzbaRadnik> list = studentskaSluzbaRepository.VratiSveRadnike();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje radnici u studentskoj službi u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.StudentskaSluzbaRadnik>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Mentor":
                    {
                        List<Entiteti.Mentor> list = mentorRepository.VratiSveMentore();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje mentori(profesori) u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.Mentor>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Šef katedre":
                    {
                        List<Entiteti.SefKatedre> list = sefKatedreRepository.VratiSveSefoveKatedri();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje šefovi katedri u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.SefKatedre>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Prodekan":
                    {
                        List<Entiteti.Prodekan> list = prodekanRepository.VratiSveProdekane();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno ne postoje prodekani u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<Entiteti.Prodekan>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                case "Diplomski rad":
                    {
                        List<View.DiplomskiView> list = diplomskiRadRepository.VratiSveDiplomskeRadove();
                        if (list.Count == 0)
                        {
                            lblIspredTabele.Text = "Trenutno nema prijavljenih diplomski radova u sistemu.";
                            dgvBrisanje.Visible = false;
                            btnBrisanje2.Visible = false;

                            return;
                        }
                        else
                        {
                            dgvBrisanje.DataSource = new BindingList<View.DiplomskiView>(list);
                            btnBrisanje2.Visible = true;
                        }
                    }
                    break;
                default: break;
            }
            dgvBrisanje.Columns["Id"].Visible = false;
            if (cmbOpcije.SelectedIndex == 2)
            {
                dgvBrisanje.Columns["MentorId"].Visible = false;
                dgvBrisanje.Columns["StudentId"].Visible = false;

                dgvBrisanje.Columns[1].HeaderText = "Ime studenta";
                dgvBrisanje.Columns[2].HeaderText = "Broj indeksa studenta";
                dgvBrisanje.Columns[5].HeaderText = "Ime mentora";
                dgvBrisanje.Columns[6].HeaderText = "Stanje diplomskog rada";
                dgvBrisanje.Columns[7].HeaderText = "Katedra";
                dgvBrisanje.Columns[8].HeaderText = "Naziv diplomskog rada";
                dgvBrisanje.Columns[9].HeaderText = "Naziv zadatka";
                dgvBrisanje.Columns[10].HeaderText = "1. član komisije";
                dgvBrisanje.Columns[11].HeaderText = "2. član komisije";
                dgvBrisanje.Columns[12].HeaderText = "Datum odbrane";
            }
            if (cmbOpcije.SelectedIndex == 1 && cmbUloga.SelectedIndex != 0)
            {
                dgvBrisanje.Columns["NalogId"].Visible = false;

                if (cmbUloga.SelectedIndex == 1)
                    dgvBrisanje.Columns[2].HeaderText = "Broj indeksa";
                dgvBrisanje.Columns[1].HeaderText = "Puno ime";
            }

            dgvBrisanje.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void cmbUloga_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbOpcije.Enabled = false;

            if (cmbOpcije.SelectedIndex == 0)
            {
                panelKreiranjeNaloga.Visible = true;
                lblMentori.Visible = false;
                dgvMentori.Visible = false;

                txtUsername.Text = "";
                txtPassword.Text = "";
                txtPunoIme.Text = "";
                txtBrojIndeksa.Text = "";
                cmbKatedre.SelectedIndex = -1;

                btnKreiraj.Visible = true;
                btnKreiraj.Text = "Kreirajte nalog";

                switch (cmbUloga.SelectedIndex)
                {
                    case 0: //admin
                        {
                            lblPunoIme.Visible = false;
                            txtPunoIme.Visible = false;
                            lblBrojIndeksailiKatedra.Visible = false;
                            txtBrojIndeksa.Visible = false;
                            cmbKatedre.Visible = false;

                            btnKreirajManji.Visible = true;
                            btnKreiraj.Visible = false;
                        }
                        break;
                    case 1: //student
                        {
                            lblPunoIme.Visible = true;
                            txtPunoIme.Visible = true;
                            lblBrojIndeksailiKatedra.Text = "Broj indeksa";
                            lblBrojIndeksailiKatedra.Visible = true;
                            txtBrojIndeksa.Visible = true;
                            cmbKatedre.Visible = false;

                            btnKreirajManji.Visible = true;
                            btnKreiraj.Visible = false;
                        }
                        break;
                    case 2: //mentor
                        {
                            lblPunoIme.Visible = true;
                            txtPunoIme.Visible = true;
                            lblBrojIndeksailiKatedra.Text = "Katedra";
                            lblBrojIndeksailiKatedra.Visible = true;
                            txtBrojIndeksa.Visible = false;
                            cmbKatedre.Visible = true;

                            btnKreirajManji.Visible = true;
                            btnKreiraj.Visible = false;
                        }
                        break;
                    case 3: //sluzba
                        {
                            lblPunoIme.Visible = true;
                            txtPunoIme.Visible = true;
                            lblBrojIndeksailiKatedra.Visible = false;
                            txtBrojIndeksa.Visible = false;
                            cmbKatedre.Visible = false;

                            btnKreirajManji.Visible = true;
                            btnKreiraj.Visible = false;
                        }
                        break;
                    case 4: //sef
                        {
                            lblMentori.Visible = true;
                            dgvMentori.Visible = true;
                            dgvMentori.AllowUserToAddRows = false;

                            btnKreirajManji.Visible = false;
                            btnKreiraj.Visible = true;

                            cmbKatedre.Visible = false;

                            List<Entiteti.Mentor> list = mentorRepository.VratiSveMentore();
                            if (list.Count > 0)
                            {
                                dgvMentori.DataSource = new BindingList<Entiteti.Mentor>(list);
                                dgvMentori.Columns["Id"].Visible = false;
                                dgvMentori.Columns["NalogId"].Visible = false;
                                dgvMentori.Columns[1].HeaderText = "Puno ime";

                                dgvMentori.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                                btnKreiraj.Text = "Postavite odabranog mentora (profesora) za šefa katedre";
                            }
                            else
                            {
                                lblMentori.Text = "Trenutno ne postoje mentori u sistemu.";
                                btnKreiraj.Visible = false;
                            }
                        }
                        break;
                    case 5: //prodekan
                        {
                            lblMentori.Visible = true;
                            dgvMentori.Visible = true;

                            btnKreirajManji.Visible = false;
                            btnKreiraj.Visible = true;

                            cmbKatedre.Visible = false;

                            List<Entiteti.Mentor> list = mentorRepository.VratiSveMentore();
                            if (list.Count > 0)
                            {
                                dgvMentori.DataSource = new BindingList<Entiteti.Mentor>(list);
                                dgvMentori.Columns["Id"].Visible = false;
                                dgvMentori.Columns["NalogId"].Visible = false;
                                dgvMentori.Columns[1].HeaderText = "Puno ime";

                                dgvMentori.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                                btnKreiraj.Text = "Postavite odabranog mentora (profesora) za prodekana";
                            }
                            else
                            {
                                lblMentori.Text = "Trenutno ne postoje mentori u sistemu.";
                                btnKreiraj.Visible = false;
                            }
                        }
                        break;
                    default: break;
                }
            }
            else if (cmbOpcije.SelectedIndex == 1)
            {
                panelKreiranjeNaloga.Visible = false;

                String uloga = cmbUloga.SelectedItem.ToString();
                PrikaziTabelu(uloga);
                panelBrisanje.Visible = true;
                btnBrisanje2.Visible = true;
            }
        }


        private void btnKreiraj_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                switch (cmbUloga.SelectedIndex)
                {
                    case 4:
                        {
                            if (dgvMentori.SelectedRows.Count == 1)
                            {
                                Guid id = ((Guid)dgvMentori.SelectedRows[0].Cells["Id"].Value);

                                if (nalogRepository.PromeniUlogu(
                                    (Guid)dgvMentori.SelectedRows[0].Cells["NalogId"].Value, "mentor-sef_katedre"))
                                {
                                    if (sefKatedreRepository.KreirajSefaKatedre(
                                        (String)dgvMentori.SelectedRows[0].Cells["PunoIme"].Value,
                                        (String)dgvMentori.SelectedRows[0].Cells["Katedra"].Value,
                                        (Guid)dgvMentori.SelectedRows[0].Cells["NalogId"].Value))
                                    {
                                        MessageBox.Show("Uspešno postavljen novi šef katedre.");
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom postavljanja novog šefa katedre. Pukušajte ponovo.");
                                }
                                else
                                    MessageBox.Show("Greška prilikom promene uloge. Pukušajte ponovo.");
                            }
                            else
                                MessageBox.Show("Morate odabrati jedan red!");
                        }
                        break;
                    case 5:
                        {
                            if (dgvMentori.SelectedRows.Count == 1)
                            {
                                Guid id = ((Guid)dgvMentori.SelectedRows[0].Cells["Id"].Value);

                                if (nalogRepository.PromeniUlogu(
                                    (Guid)dgvMentori.SelectedRows[0].Cells["NalogId"].Value, "mentor-prodekan"))
                                {
                                    if (prodekanRepository.KreirajProdekana(
                                        (String)dgvMentori.SelectedRows[0].Cells["PunoIme"].Value,
                                        (Guid)dgvMentori.SelectedRows[0].Cells["NalogId"].Value))
                                    {
                                        MessageBox.Show("Uspešno postavljen novi prodekan.");
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom postavljanja novog prodekana. Pukušajte ponovo.");
                                }
                                else
                                    MessageBox.Show("Greška prilikom promene uloge. Pukušajte ponovo.");
                            }
                            else
                                MessageBox.Show("Morate odabrati jedan red!");
                        }
                        break;
                    default: break;
                }
            }
        }

        private void btnNazad_Click(object sender, EventArgs e)
        {
            cmbOpcije.SelectedIndex = -1;
            cmbUloga.SelectedIndex = -1;

            cmbOpcije.Enabled = true;

            panelKreiranjeNaloga.Visible = false;
            panelBrisanje.Visible = false;
            panelKatedre.Visible = false;

            btnNazad.Visible = false;
        }

        private void Admin_FormClosed(object sender, FormClosedEventArgs e)
        {
            prijavaForm.Show();
        }

        private void btnKatedre_Click(object sender, EventArgs e)
        {
            if (txtKatedra.Text == "")
                MessageBox.Show("Unesite naziv katedre.");
            else
            {
                DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    if (adminRepository.KreirajKatedru(txtKatedra.Text))
                    {
                        MessageBox.Show("Uspešno kreirana katedra.");
                        cmbKatedre.Items.Add(txtKatedra.Text);
                    }
                    else
                        MessageBox.Show("Greška prilikom kreiranja katedre. Nazivi katedri moraju biti jedinstveni.");

                    txtKatedra.Text = "";
                }
            }
        }

        private void btnKreirajManji_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
            if (dialogResult == DialogResult.OK)
            {
                switch (cmbUloga.SelectedIndex)
                {
                    case 0:
                        {
                            if (txtUsername.Text == "" || txtPassword.Text == "")
                            {
                                MessageBox.Show("Popunite sva polja!");
                                break;
                            }

                            int ret = adminRepository.KreirajAdmina(txtUsername.Text, txtPassword.Text);
                            if (ret == 2)
                            {
                                MessageBox.Show("Uspešno kreiran admin nalog.");
                                txtUsername.Text = "";
                                txtPassword.Text = "";
                            }
                            else if (ret == 3)
                                MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            else
                            {
                                MessageBox.Show("Već postoji nalog sa datim korisničkim imenom. " +
                                    "Unesite nove vrednosti i pokušajte ponovo.");
                                txtUsername.Text = "";
                                txtPassword.Text = "";
                            }
                        }
                        break;
                    case 1:
                        {
                            if (txtUsername.Text == "" || txtPassword.Text == "" ||
                                txtPunoIme.Text == "" || txtBrojIndeksa.Text == "")
                            {
                                MessageBox.Show("Popunite sva polja!");
                                break;
                            }

                            int ret = nalogRepository.KreirajNalog(txtUsername.Text, txtPassword.Text, "student");

                            if (ret == 2)
                            {
                                Guid nalogId = nalogRepository.VratiIdNaloga(txtUsername.Text, txtPassword.Text);

                                if (studentRepository.KreirajStudenta(txtPunoIme.Text, txtBrojIndeksa.Text, nalogId))
                                {
                                    MessageBox.Show("Uspešno kreiran student nalog.");
                                    txtUsername.Text = "";
                                    txtPassword.Text = "";
                                    txtPunoIme.Text = "";
                                    txtBrojIndeksa.Text = "";
                                }
                                else
                                    MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            }
                            else if (ret == 3)
                                MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            else
                            {
                                MessageBox.Show("Već postoji nalog sa datim korisničkim imenom. " +
                                    "Unesite nove vrednosti i pokušajte ponovo.");
                                txtUsername.Text = "";
                                txtPassword.Text = "";
                            }
                        }
                        break;
                    case 2:
                        {
                            if (txtUsername.Text == "" || txtPassword.Text == "" ||
                                txtPunoIme.Text == "" || cmbKatedre.SelectedIndex == -1)
                            {
                                MessageBox.Show("Popunite sva polja!");
                                break;
                            }

                            int ret = nalogRepository.KreirajNalog(txtUsername.Text, txtPassword.Text, "mentor");

                            if (ret == 2)
                            {
                                Guid nalogId = nalogRepository.VratiIdNaloga(txtUsername.Text, txtPassword.Text);

                                if (mentorRepository.KreirajMentora(txtPunoIme.Text,
                                    cmbKatedre.SelectedItem.ToString(), nalogId))
                                {
                                    MessageBox.Show("Uspešno kreiran mentor nalog.");
                                    txtUsername.Text = "";
                                    txtPassword.Text = "";
                                    txtPunoIme.Text = "";
                                    cmbKatedre.SelectedIndex = -1;
                                }
                                else
                                    MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            }
                            else if (ret == 3)
                                MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            else
                            {
                                MessageBox.Show("Već postoji nalog sa datim korisničkim imenom. " +
                                    "Unesite nove vrednosti i pokušajte ponovo.");
                                txtUsername.Text = "";
                                txtPassword.Text = "";
                            }
                        }
                        break;
                    case 3:
                        {
                            if (txtUsername.Text == "" || txtPassword.Text == "")
                            {
                                MessageBox.Show("Popunite sva polja!");
                                break;
                            }

                            int ret = nalogRepository.KreirajNalog(txtUsername.Text, txtPassword.Text, "studentska_sluzba_radnik");

                            if (ret == 2)
                            {
                                Guid nalogId = nalogRepository.VratiIdNaloga(txtUsername.Text, txtPassword.Text);

                                if (studentskaSluzbaRepository.KreirajRadnika(txtPunoIme.Text, nalogId))
                                {
                                    MessageBox.Show("Uspešno kreiran radnik nalog.");
                                    txtUsername.Text = "";
                                    txtPassword.Text = "";
                                    txtPunoIme.Text = "";
                                }
                                else
                                    MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            }
                            else if (ret == 3)
                                MessageBox.Show("Greška prilikom kreiranja naloga. Pukušajte ponovo.");
                            else
                            {
                                MessageBox.Show("Već postoji nalog sa datim korisničkim imenom. " +
                                    "Unesite nove vrednosti i pokušajte ponovo.");
                                txtUsername.Text = "";
                                txtPassword.Text = "";
                            }
                        }
                        break;
                }
            }
        }

        private void btnBrisanje2_Click_1(object sender, EventArgs e)
        {
            if (dgvBrisanje.SelectedRows.Count == 1)
            {
                DialogResult dialogResult = MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    Guid id = ((Guid)dgvBrisanje.SelectedRows[0].Cells["Id"].Value);

                    if (cmbOpcije.SelectedIndex == 2)
                    {
                        if (diplomskiRadRepository.ObrisiRad(id))
                        {
                            MessageBox.Show("Ušpesno izvršeno brisanje.");
                            PrikaziTabelu("Diplomski rad");
                        }
                        else
                            MessageBox.Show("Greška prilikom brisanja.");
                    }
                    else
                    {
                        String tabela = cmbUloga.SelectedItem.ToString();

                        switch (tabela)
                        {
                            case "Administrator":
                                {
                                    if (adminRepository.ObrisiAdmina(id))
                                    {
                                        MessageBox.Show("Ušpesno izvršeno brisanje.");
                                        PrikaziTabelu(tabela);
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja.");
                                }
                                break;
                            case "Student":
                                {
                                    if (adminRepository.Obrisi("student", id))
                                    {
                                        MessageBox.Show("Uspešno izvršeno brisanje.");
                                        PrikaziTabelu(tabela);
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja, " +
                                            "verovatno zato što je ovaj student povezan sa nekim diplomski radom.");
                                }
                                break;
                            case "Radnik u studentskoj službi":
                                {
                                    if (adminRepository.Obrisi("studentska_sluzba_radnik", id))
                                    {
                                        MessageBox.Show("Ušpesno izvršeno brisanje.");
                                        PrikaziTabelu(tabela);
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja.");
                                }
                                break;
                            case "Mentor":
                                {
                                    if (adminRepository.Obrisi("mentor", id))
                                    {
                                        MessageBox.Show("Uspešno izvršeno brisanje.");
                                        PrikaziTabelu(tabela);
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja, " +
                                            "verovatno zato što je ovaj mentor povezan sa nekim diplomski radom.");
                                }
                                break;
                            case "Šef katedre":
                                {
                                    if (adminRepository.Obrisi("sef_katedre", id))
                                    {
                                        if (nalogRepository.PromeniUlogu((Guid)dgvBrisanje.SelectedRows[0].Cells["NalogId"].Value,
                                            "mentor"))
                                        {
                                            MessageBox.Show("Ušpesno izvršeno brisanje.");
                                            PrikaziTabelu(tabela);
                                        }
                                        else
                                            MessageBox.Show("Greška prilikom promene uloge.");
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja.");
                                }
                                break;
                            case "Prodekan":
                                {
                                    if (adminRepository.Obrisi("prodekan", id))
                                    {
                                        if (nalogRepository.PromeniUlogu((Guid)dgvBrisanje.SelectedRows[0].Cells["NalogId"].Value,
                                            "mentor"))
                                        {
                                            MessageBox.Show("Uspešno izvršeno brisanje.");
                                            PrikaziTabelu(tabela);
                                        }
                                        else
                                            MessageBox.Show("Greška prilikom promene uloge.");
                                    }
                                    else
                                        MessageBox.Show("Greška prilikom brisanja.");
                                }
                                break;
                            default: break;
                        }
                    }
                }
            }
            else
                MessageBox.Show("Morate odabrati jedan red!");
        }
    }
}
