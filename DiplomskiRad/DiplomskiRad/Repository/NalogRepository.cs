﻿using System;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class NalogRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        public int KreirajNalog(String username, String password, String uloga)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("select id from diplomski.nalog " +
                "where username='" + username + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    conn.Close();
                    return 1;
                }

            using (var cmd = new NpgsqlCommand("insert into diplomski.nalog (username, password, uloga) " +
                "values ('" + username + "', '" + password + "', '" + uloga + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return 2;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return 3;
                }
        }

        public Guid VratiIdNaloga(String username, String password)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            Guid id = new Guid();

            conn.Open();

            using (var cmd = new NpgsqlCommand("select id from diplomski.nalog " +
                "where username='" + username + "' and password='" + password + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    id = (Guid)reader.GetValue(0);
                }

            conn.Close();
            return id;
        }

        public bool PromeniUlogu(Guid id, String novaUloga)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("update diplomski.nalog set uloga='"+ novaUloga +"'" +
                "where id='"+ id +"'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public bool ObrisiNalog(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("delete from diplomski.nalog " +
                "where id='"+ id +"'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }
    }
}
