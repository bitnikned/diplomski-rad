﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class StudentskaSluzbaRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        //public List<Entiteti.StudentskaSluzbaRadnik> VratiSveMentore()
        //{
        //    var conn = new NpgsqlConnection(builder.getConnectionString());

        //    conn.Open();

        //    List<Entiteti.Mentor> mentori = new List<Entiteti.Mentor>();

        //    using (var cmd = new NpgsqlCommand("select * from diplomski.mentor", conn))
        //    using (var reader = cmd.ExecuteReader())
        //        while (reader.Read())
        //        {
        //            Entiteti.Mentor mentor = new Entiteti.Mentor();
        //            mentor.Id = (Guid)reader.GetValue(0);
        //            mentor.PunoIme = (String)reader.GetValue(1);
        //            mentor.Katedra = (String)reader.GetValue(2);

        //            mentori.Add(mentor);
        //        }

        //    conn.Close();
        //    return mentori;
        //}

        public bool KreirajRadnika(String punoIme, Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.studentska_sluzba_radnik (puno_ime, nalog_id) " +
                "values ('" + punoIme + "', '" + nalogId + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
            conn.Close();
            return true;
        }

        public List<Entiteti.StudentskaSluzbaRadnik> VratiSveRadnike()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());
            List<Entiteti.StudentskaSluzbaRadnik> radnici = new List<Entiteti.StudentskaSluzbaRadnik>();

            conn.Open();

            using (var cmd = new NpgsqlCommand("select * from diplomski.studentska_sluzba_radnik", conn))
            using (var reader = cmd.ExecuteReader())
                while(reader.Read())
                {
                    Entiteti.StudentskaSluzbaRadnik radnik = new Entiteti.StudentskaSluzbaRadnik();

                    radnik.Id = (Guid)reader.GetValue(0);
                    radnik.PunoIme = (String)reader.GetValue(1);
                    radnik.NalogId = (Guid)reader.GetValue(2);

                    radnici.Add(radnik);
                }

            conn.Close();
            return radnici;
        }

        public String VratiImeRadnika(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String ime;

            using (var cmd = new NpgsqlCommand("select puno_ime from diplomski.studentska_sluzba_radnik " +
                "where nalog_id='" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    ime = (String)reader.GetValue(0);
                    conn.Close();
                    return ime;
                }

            conn.Close();
            return null;
        }
    }
}
