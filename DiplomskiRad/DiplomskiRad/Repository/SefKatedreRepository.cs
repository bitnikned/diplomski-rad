﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class SefKatedreRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();
        NalogRepository nalogRepository = new NalogRepository();

        public String VratiNazivKatedre(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String katedra;

            using (var cmd = new NpgsqlCommand("select katedra from diplomski.sef_katedre " +
                            "where nalog_id = '" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    katedra = (String)reader.GetValue(0);
                    conn.Close();
                    return katedra;
                }
                else
                {
                    conn.Close();
                    return null;
                }
        }

        private bool DeaktivirajSefaKatedreAkoPostoji(Guid nalogId, String katedra)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            bool postoji = false;
            Guid nalog = new Guid();

            using (var cmd = new NpgsqlCommand("select nalog_id from diplomski.sef_katedre " +
                            "where katedra='" + katedra + "' and aktivan=true", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    nalog = (Guid)reader.GetValue(0);
                    postoji = true;
                    conn.Close();
                    if (nalog == nalogId)
                        return true;
                }
                else
                {
                    conn.Close();
                    return true;
                }

            if (postoji)
            {
                nalogRepository.PromeniUlogu(nalog, "mentor");

                conn.Open();

                using (var cmd = new NpgsqlCommand("update diplomski.sef_katedre set aktivan=false where nalog_id='" + nalog + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }
            }
            return true;
        }

        private int PostaviOpetAkoJeVecBioSefKatedre(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            bool bio = false;
            conn.Open();

            using (var cmd = new NpgsqlCommand("select id from diplomski.sef_katedre " +
                            "where nalog_id='" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    conn.Close();
                    bio = true;
                }
                else
                {
                    conn.Close();
                }
            conn.Close();

            if (bio)
            {
                nalogRepository.PromeniUlogu(nalogId, "mentor-sef_katedre");

                conn.Open();

                using (var cmd = new NpgsqlCommand("update diplomski.sef_katedre set aktivan=true " +
                                "where nalog_id = '" + nalogId + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        return 1;
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return 2;
                    }
            }
            return 3;
        }

        public bool KreirajSefaKatedre(String punoIme, String katedra, Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());
            if (DeaktivirajSefaKatedreAkoPostoji(nalogId, katedra))
            {
                int ret = PostaviOpetAkoJeVecBioSefKatedre(nalogId);

                if (ret == 3)
                {
                    conn.Open();

                    using (var cmd = new NpgsqlCommand("insert into diplomski.sef_katedre (puno_ime, katedra, nalog_id, aktivan) " +
                        "values ('" + punoIme + "', '" + katedra + "', '" + nalogId + "', true)", conn))
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception exc)
                        {
                            conn.Close();
                            return false;
                        }
                    conn.Close();
                    return true;
                }
                else if (ret == 1)
                    return true;
            }
            return false;
        }

        public List<Entiteti.SefKatedre> VratiSveSefoveKatedri()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.SefKatedre> sefovi = new List<Entiteti.SefKatedre>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.sef_katedre order by katedra", conn))
            using (var reader = cmd.ExecuteReader())
                while(reader.Read())
                {
                    Entiteti.SefKatedre sef = new Entiteti.SefKatedre();

                    sef.Id = (Guid)reader.GetValue(0);
                    sef.PunoIme = (String)reader.GetValue(1);
                    sef.Katedra = (String)reader.GetValue(2);
                    sef.NalogId = (Guid)reader.GetValue(3);
                    sef.Aktivan = ((bool)reader.GetValue(4) ? "da" : "ne");

                    sefovi.Add(sef);
                }

            conn.Close();
            return sefovi;
        }

        public String VratiImeSefaKatedre(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String ime;

            using (var cmd = new NpgsqlCommand("select puno_ime from diplomski.sef_katedre " +
                "where id='" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    ime = (String)reader.GetValue(0);
                    conn.Close();
                    return ime;
                }

            conn.Close();
            return null;
        }
    }
}
