﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class StudentRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        public Entiteti.Student VratiStudenta(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("select id, puno_ime, broj_indeksa from diplomski.student " +
                "where nalog_id = '" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    Entiteti.Student student = new Entiteti.Student();

                    student.Id = (Guid)reader.GetValue(0);
                    student.PunoIme = (String)reader.GetValue(1);
                    student.BrojIndeksa = (String)reader.GetValue(2);

                    conn.Close();
                    return student;
                }
                else
                {
                    conn.Close();
                    return null;
                }
        }

        public View.StudentView VratiStudentaView(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("select puno_ime, broj_indeksa from diplomski.student " +
                "where id = '" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    View.StudentView studentView = new View.StudentView();

                    studentView.PunoIme = (String)reader.GetValue(0);
                    studentView.BrojIndeksa = (String)reader.GetValue(1);

                    conn.Close();
                    return studentView;
                }
                else
                {
                    conn.Close();
                    return null;
                }
        }

        public bool KreirajStudenta(String punoIme, String brojIndeksa, Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.student (puno_ime, broj_indeksa, nalog_id) " +
                "values ('" + punoIme + "', '" + brojIndeksa + "', '" + nalogId + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
            conn.Close();
            return true;
        }

        public List<Entiteti.Student> VratiSveStudente()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());
            List<Entiteti.Student> studenti = new List<Entiteti.Student>();

            conn.Open();

            using (var cmd = new NpgsqlCommand("select * from diplomski.student order by broj_indeksa desc", conn))
            using (var reader = cmd.ExecuteReader())
                while(reader.Read())
                {
                    Entiteti.Student student = new Entiteti.Student();

                    student.Id = (Guid)reader.GetValue(0);
                    student.PunoIme = (String)reader.GetValue(1);
                    student.BrojIndeksa = (String)reader.GetValue(2);
                    student.NalogId = (Guid)reader.GetValue(3);

                    studenti.Add(student);
                }

            conn.Close();
            return studenti;
        }
    }
}
