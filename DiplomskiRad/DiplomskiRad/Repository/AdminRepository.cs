﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class AdminRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        public bool KreirajDefaultAdmina()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            bool postoji = false;

            using (var cmd = new NpgsqlCommand("select * from diplomski.nalog where username='adm' and password='adm'", conn))
            using (var reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                    postoji = true;
            }

            conn.Close();

            if (!postoji)
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand("insert into diplomski.nalog (username, password, uloga) values ('adm', 'adm', 'admin')", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }

                conn.Close();
            }

            return true;
        }

        public int KreirajAdmina(String username, String password)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();
            using (var cmd = new NpgsqlCommand("select id from diplomski.nalog " +
                "where username='" + username + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    conn.Close();
                    return 1;
                }

            using (var cmd = new NpgsqlCommand("insert into diplomski.nalog (username, password, uloga) " +
                    "values ('" + username + "', '" + password + "', 'admin')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return 2;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return 3;
                }
        }

        public List<Entiteti.Admin> VratiSveAdmine()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.Admin> admini = new List<Entiteti.Admin>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.nalog where uloga='admin'", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    Entiteti.Admin admin = new Entiteti.Admin();

                    admin.Id = (Guid)reader.GetValue(0);
                    admin.Username = (String)reader.GetValue(2);
                    admin.Password = (String)reader.GetValue(3);

                    admini.Add(admin);
                }

            conn.Close();
            return admini;
        }

        public bool ObrisiAdmina(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("delete from diplomski.nalog where id='" + id + "'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }

            conn.Close();
            return true;
        }

        private Guid VratiNalogId(String tabela, Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            Guid nalogId = new Guid();

            using (var cmd = new NpgsqlCommand("select nalog_id from diplomski." + tabela +
                " where id='" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    nalogId = (Guid)reader.GetValue(0);
                }

            conn.Close();
            return nalogId;
        }

        public bool Obrisi(String tabela, Guid id)
        {
            Guid nalogId = VratiNalogId(tabela, id);

            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("delete from diplomski." + tabela + " where id='" + id + "'", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }

            if(tabela == "mentor")
            {
                using (var cmd = new NpgsqlCommand("delete from diplomski.sef_katedre where nalog_id='" + nalogId + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }

                using (var cmd = new NpgsqlCommand("delete from diplomski.prodekan where nalog_id='" + nalogId + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }
            }

            if (tabela != "sef_katedre" && tabela != "prodekan")
            {
                using (var cmd = new NpgsqlCommand("delete from diplomski.nalog where id='" + nalogId + "'", conn))
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception exc)
                    {
                        conn.Close();
                        return false;
                    }
            }

            conn.Close();
            return true;
        }

        public bool KreirajKatedru(String katedra)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.katedra (katedra) " +
                    "values ('" + katedra + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
        }

        public List<String> VratiSveKatedre()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<String> katedre = new List<String>();

            using (var cmd = new NpgsqlCommand("select katedra from diplomski.katedra", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    katedre.Add((String)reader.GetValue(0));
                }

            conn.Close();
            return katedre;
        }
    }
}
