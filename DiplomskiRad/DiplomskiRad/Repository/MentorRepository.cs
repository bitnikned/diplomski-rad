﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace DiplomskiRad.Repository
{
    class MentorRepository
    {
        ConnectionStringBuilder builder = new ConnectionStringBuilder();

        public List<Entiteti.Mentor> VratiSveMentore()
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<Entiteti.Mentor> mentori = new List<Entiteti.Mentor>();

            using (var cmd = new NpgsqlCommand("select * from diplomski.mentor order by katedra", conn))
            using (var reader = cmd.ExecuteReader())
                while(reader.Read())
                {
                    Entiteti.Mentor mentor = new Entiteti.Mentor();

                    mentor.Id = (Guid)reader.GetValue(0);
                    mentor.PunoIme = (String)reader.GetValue(1);
                    mentor.Katedra = (String)reader.GetValue(2);
                    mentor.NalogId = (Guid)reader.GetValue(3);

                    mentori.Add(mentor);
                }

            conn.Close();
            return mentori;
        }

        public List<View.MentorView> VratiSveMentoreKatedre(String katedra)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<View.MentorView> mentoriView = new List<View.MentorView>();

            using (var cmd = new NpgsqlCommand("select id, puno_ime from diplomski.mentor " +
                "where katedra='"+ katedra +"'", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    View.MentorView mentorView = new View.MentorView();
                    mentorView.Id = (Guid)reader.GetValue(0);
                    mentorView.PunoIme = (String)reader.GetValue(1);
                    mentoriView.Add(mentorView);
                }

            conn.Close();
            return mentoriView;
        }

        public List<View.MentorView> VratiClanoveKomisijeKatedre(String katedra, Guid mentorId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            List<View.MentorView> mentoriView = new List<View.MentorView>();

            using (var cmd = new NpgsqlCommand("select id, puno_ime from diplomski.mentor " +
                "where katedra='" + katedra + "' and id<>'" + mentorId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    View.MentorView mentorView = new View.MentorView();
                    mentorView.Id = (Guid)reader.GetValue(0);
                    mentorView.PunoIme = (String)reader.GetValue(1);
                    mentoriView.Add(mentorView);
                }

            conn.Close();
            return mentoriView;
        }

        public String VratiKatedruMentora(Guid mentorId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String katedra;

            using (var cmd = new NpgsqlCommand("select katedra from diplomski.mentor " +
                "where id='"+ mentorId +"'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    katedra = (String)reader.GetValue(0);
                    conn.Close();
                    return katedra;
                }

            conn.Close();
            return null;
        }

        public Guid VratiIdMentora(Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            Guid id = new Guid();

            using (var cmd = new NpgsqlCommand("select id from diplomski.mentor " +
                "where nalog_id='" + nalogId + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    id = (Guid)reader.GetValue(0);
                    conn.Close();
                    return id;
                }

            conn.Close();
            return id;
        }

        public String VratiImeMentora(Guid id)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            String ime;

            using (var cmd = new NpgsqlCommand("select puno_ime from diplomski.mentor " +
                "where id='" + id + "'", conn))
            using (var reader = cmd.ExecuteReader())
                if (reader.Read())
                {
                    ime = (String)reader.GetValue(0);
                    conn.Close();
                    return ime;
                }

            conn.Close();
            return null;
        }

        public bool KreirajMentora(String punoIme, String katedra, Guid nalogId)
        {
            var conn = new NpgsqlConnection(builder.getConnectionString());

            conn.Open();

            using (var cmd = new NpgsqlCommand("insert into diplomski.mentor (puno_ime, katedra, nalog_id) " +
                "values ('" + punoIme + "', '" + katedra + "', '" + nalogId + "')", conn))
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception exc)
                {
                    conn.Close();
                    return false;
                }
            conn.Close();
            return true;
        }
    }
}
