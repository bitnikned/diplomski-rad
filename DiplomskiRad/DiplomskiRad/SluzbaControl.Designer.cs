﻿namespace DiplomskiRad
{
    partial class SluzbaControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDiplomskiRadovi = new System.Windows.Forms.DataGridView();
            this.lblIspredTabele = new System.Windows.Forms.Label();
            this.btnOpcije = new System.Windows.Forms.Button();
            this.lblUloga = new System.Windows.Forms.Label();
            this.cmbStanja = new System.Windows.Forms.ComboBox();
            this.lblOdabirStanja = new System.Windows.Forms.Label();
            this.btnPrikazKomisije = new System.Windows.Forms.Button();
            this.lblKomisija = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiplomskiRadovi)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDiplomskiRadovi
            // 
            this.dgvDiplomskiRadovi.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvDiplomskiRadovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiplomskiRadovi.Location = new System.Drawing.Point(4, 155);
            this.dgvDiplomskiRadovi.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDiplomskiRadovi.Name = "dgvDiplomskiRadovi";
            this.dgvDiplomskiRadovi.Size = new System.Drawing.Size(979, 300);
            this.dgvDiplomskiRadovi.TabIndex = 1;
            this.dgvDiplomskiRadovi.SelectionChanged += new System.EventHandler(this.dgvDiplomskiRadovi_SelectionChanged);
            // 
            // lblIspredTabele
            // 
            this.lblIspredTabele.AutoSize = true;
            this.lblIspredTabele.Location = new System.Drawing.Point(133, 155);
            this.lblIspredTabele.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIspredTabele.Name = "lblIspredTabele";
            this.lblIspredTabele.Size = new System.Drawing.Size(175, 19);
            this.lblIspredTabele.TabIndex = 2;
            this.lblIspredTabele.Text = "Prijavljeni diplomski radovi:";
            // 
            // btnOpcije
            // 
            this.btnOpcije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnOpcije.Location = new System.Drawing.Point(137, 498);
            this.btnOpcije.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpcije.Name = "btnOpcije";
            this.btnOpcije.Size = new System.Drawing.Size(149, 71);
            this.btnOpcije.TabIndex = 3;
            this.btnOpcije.Text = "Opcije";
            this.btnOpcije.UseVisualStyleBackColor = false;
            this.btnOpcije.Click += new System.EventHandler(this.btnOpcije_Click);
            // 
            // lblUloga
            // 
            this.lblUloga.AutoSize = true;
            this.lblUloga.Location = new System.Drawing.Point(18, 9);
            this.lblUloga.Name = "lblUloga";
            this.lblUloga.Size = new System.Drawing.Size(117, 19);
            this.lblUloga.TabIndex = 5;
            this.lblUloga.Text = "Studentska sluzba";
            // 
            // cmbStanja
            // 
            this.cmbStanja.FormattingEnabled = true;
            this.cmbStanja.Location = new System.Drawing.Point(425, 61);
            this.cmbStanja.Name = "cmbStanja";
            this.cmbStanja.Size = new System.Drawing.Size(228, 27);
            this.cmbStanja.TabIndex = 6;
            this.cmbStanja.SelectedIndexChanged += new System.EventHandler(this.cmbStanja_SelectedIndexChanged);
            // 
            // lblOdabirStanja
            // 
            this.lblOdabirStanja.AutoSize = true;
            this.lblOdabirStanja.Location = new System.Drawing.Point(128, 69);
            this.lblOdabirStanja.Name = "lblOdabirStanja";
            this.lblOdabirStanja.Size = new System.Drawing.Size(282, 19);
            this.lblOdabirStanja.TabIndex = 7;
            this.lblOdabirStanja.Text = "Odaberite stanje diplomskih radova za prikaz";
            // 
            // btnPrikazKomisije
            // 
            this.btnPrikazKomisije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPrikazKomisije.Location = new System.Drawing.Point(327, 498);
            this.btnPrikazKomisije.Name = "btnPrikazKomisije";
            this.btnPrikazKomisije.Size = new System.Drawing.Size(119, 71);
            this.btnPrikazKomisije.TabIndex = 8;
            this.btnPrikazKomisije.Text = "Članovi komisije";
            this.btnPrikazKomisije.UseVisualStyleBackColor = false;
            this.btnPrikazKomisije.Click += new System.EventHandler(this.btnPrikazKomisije_Click);
            // 
            // lblKomisija
            // 
            this.lblKomisija.AutoSize = true;
            this.lblKomisija.Location = new System.Drawing.Point(471, 498);
            this.lblKomisija.Name = "lblKomisija";
            this.lblKomisija.Size = new System.Drawing.Size(113, 19);
            this.lblKomisija.TabIndex = 9;
            this.lblKomisija.Text = "Clanovi komisije ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.lblUloga);
            this.panel1.Location = new System.Drawing.Point(831, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(153, 38);
            this.panel1.TabIndex = 10;
            // 
            // SluzbaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblKomisija);
            this.Controls.Add(this.btnPrikazKomisije);
            this.Controls.Add(this.lblOdabirStanja);
            this.Controls.Add(this.cmbStanja);
            this.Controls.Add(this.btnOpcije);
            this.Controls.Add(this.lblIspredTabele);
            this.Controls.Add(this.dgvDiplomskiRadovi);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SluzbaControl";
            this.Size = new System.Drawing.Size(987, 620);
            this.Load += new System.EventHandler(this.SluzbaControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiplomskiRadovi)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvDiplomskiRadovi;
        private System.Windows.Forms.Label lblIspredTabele;
        private System.Windows.Forms.Button btnOpcije;
        private System.Windows.Forms.Label lblUloga;
        private System.Windows.Forms.ComboBox cmbStanja;
        private System.Windows.Forms.Label lblOdabirStanja;
        private System.Windows.Forms.Button btnPrikazKomisije;
        private System.Windows.Forms.Label lblKomisija;
        private System.Windows.Forms.Panel panel1;
    }
}
