﻿namespace DiplomskiRad
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.npgsqlCommandBuilder1 = new Npgsql.NpgsqlCommandBuilder();
            this.mentorControl1 = new DiplomskiRad.MentorControl();
            this.sluzbaControl1 = new DiplomskiRad.SluzbaControl();
            this.SuspendLayout();
            // 
            // npgsqlCommandBuilder1
            // 
            this.npgsqlCommandBuilder1.QuotePrefix = "\"";
            this.npgsqlCommandBuilder1.QuoteSuffix = "\"";
            // 
            // mentorControl1
            // 
            this.mentorControl1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.mentorControl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mentorControl1.Location = new System.Drawing.Point(0, 1);
            this.mentorControl1.Margin = new System.Windows.Forms.Padding(4);
            this.mentorControl1.Name = "mentorControl1";
            this.mentorControl1.Size = new System.Drawing.Size(987, 625);
            this.mentorControl1.TabIndex = 1;
            // 
            // sluzbaControl1
            // 
            this.sluzbaControl1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sluzbaControl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sluzbaControl1.Location = new System.Drawing.Point(0, 1);
            this.sluzbaControl1.Margin = new System.Windows.Forms.Padding(4);
            this.sluzbaControl1.Name = "sluzbaControl1";
            this.sluzbaControl1.Size = new System.Drawing.Size(987, 620);
            this.sluzbaControl1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(988, 622);
            this.Controls.Add(this.sluzbaControl1);
            this.Controls.Add(this.mentorControl1);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diplomski rad";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion
        private Npgsql.NpgsqlCommandBuilder npgsqlCommandBuilder1;
        private MentorControl mentorControl1;
        private SluzbaControl sluzbaControl1;
    }
}

